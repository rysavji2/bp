import Bachelor.Editor.Manipulation.Html.CreateHtmlInfo;
import Bachelor.Editor.UI.Menu.MenuView;
import Bachelor.Editor.UI.Title.Title;

import javax.swing.*;

/**
 * Funkce main pro zavedení do aplikace
 */
public class Main {
    /**
     * Spouštěcí funkce aplikace.
     * @param args argumenty zadané z příkazové řádky.
     */
    public static void main(String[] args) {
        // Otevření okna menu.
        SwingUtilities.invokeLater(() -> {
            MenuView window = new MenuView();
            window.setVisible(true);
        });
        /*SwingUtilities.invokeLater(() -> {
            Title t = new Title("Hey", MenuView.Language.CZ);
            t.setVisible(true);
        });*/
    }
}
