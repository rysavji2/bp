/*
    Edge.java
    Jiří Ryšavý
    11.07.2019
    Editor modelu Pražského hradu. (Bachelor Thesis)
*/
package Bachelor.Editor.Graph;

/**
 * Třída reprezentující hranu grafu.
 */
public class Edge{
    long from, to;

    /**
     * Konstruktor hrany.
     * @param from Index počátečního bodu hrany
     * @param to Index cílového bodu hrany
     */
    public Edge(long from, long to) {
        this.from = from;
        this.to = to;
    }

    //Následují gettery pro hranu.

    public long getFrom() {
        return from;
    }

    public long getTo() {
        return to;
    }
}
