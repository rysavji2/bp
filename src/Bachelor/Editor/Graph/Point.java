/*
    Point.java
    Jiří Ryšavý
    11.07.2019
    Editor modelu Pražského hradu. (Bachelor Thesis)
*/
package Bachelor.Editor.Graph;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

/**
 * Třída reprezentující bod grafu.
 */
public class Point{
    private double x, y, z;

    /**
     * Konstruktor bodu grafu.
     * @param x souřadnice x ve světě modelu.
     * @param y souřadnice y ve světě modelu.
     * @param z souřadnice z ve světě modelu.
     */
    public Point(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Počítá vzdálenost dvou bodů s pomocí euklidovské vzdálenosti.
     * @param b bod, se kterým se kterým chceme vědět vzdálenost vůči aktuálnímu.
     * @return vzdálenost dvou bodů.
     */
    public double distance(Point b){
        return sqrt(pow(x-b.x, 2) + pow(y-b.y, 2) + pow(z-b.z, 2));
    }

    //Následují gettery pru jednotlivé části bodu.

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }
}
