/*
    ServerConection.java
    Jiří Ryšavý
    11.07.2019
    Editor modelu Pražského hradu. (Bachelor Thesis)
*/
package Bachelor.Editor.Conection;

import Bachelor.Editor.UI.Menu.MenuView;
import org.apache.commons.net.ftp.FTPClient;

import javax.swing.*;
import java.io.*;

/**
 * Třída pro spojení se serverem a stahování/odesílání souborů
 */
public class ServerConection {
    //IPv4 adresa serveru
    private static final String FTP_SERVER = "127.0.0.1";
    //Uživatelské jméno pro FTP přístup
    private static final String USERNAME = "admin";
    //Heslo pro přístup k FTP serveru
    private static final String PASSWORD = "admin";
    //Port na kterém běží FTP komunikace
    private static final Integer PORT = 21;

    //Cesta k souboru na serveru
    private String serverFile;
    //Cesta k souboru na lokálním stroji
    private String localFile;
    private MenuView.Language language;
    private JFrame menu;

    /**
     * Konstruktor třídy
     * @param serverFile cesta k souboru na serveru
     * @param localFile cesta k souboru na lokálním stroji
     */
    public ServerConection(String serverFile, String localFile) {
        this.serverFile = serverFile;
        this.localFile = localFile;
        this.language = language;
        this.menu = menu;
    }

    /**
     * Stahování textových souborů ze serveru.
     * Vytvoří spojení se serverem a přenese soubor (z cesty zadanou jako sercverovou na lokální).
     * V případě chyby spojení, nebo stahování se spojení ukončí a odhlásí uživatele FTP serveru.
     */
    public void downloadFromServer() {
        FTPClient client = new FTPClient();

        try {
            client.connect(FTP_SERVER, PORT);
            client.login(USERNAME, PASSWORD);

            File localfile = new File(localFile);

            OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(localfile));
            client.retrieveFile(serverFile, outputStream);
            outputStream.close();

        }
        catch (IOException ex) {
            if (language.equals(MenuView.Language.CZ))
                JOptionPane.showMessageDialog(menu, "Hodnoty v Rotaci, Posunu a Zvětšení musí být typu double.");
            else
                JOptionPane.showMessageDialog(menu, "Values in Translation, Rotation and Scale must be of type double.");
//            System.out.println("Error occurs in downloading files from ftp Server : " + ex.getMessage());
        }
        finally {
            try {
                if (client.isConnected()) {
                    client.logout();
                    client.disconnect();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * Odesílání textových souborů na server.
     * Vytvoří spojení se serverem a přenese soubor (z cesty zadanou jako lokální na serverovou).
     * V případě chyby spojení, nebo stahování se spojení ukončí a odhlásí uživatele FTP serveru.
     */
    public void sendToServer(){
        FTPClient client = new FTPClient();

        try {
            client.connect(FTP_SERVER, PORT);
            client.login(USERNAME, PASSWORD);
            client.setBufferSize(2048000);

            FileInputStream fis = new FileInputStream(localFile);
            client.storeFile(serverFile, fis);
            fis.close();

        } catch (IOException ex) {
            if (language.equals(MenuView.Language.CZ))
                JOptionPane.showMessageDialog(menu, "Hodnoty v Rotaci, Posunu a Zvětšení musí být typu double.");
            else
                JOptionPane.showMessageDialog(menu, "Values in Translation, Rotation and Scale must be of type double.");
//            System.out.println("Error occurs in downloading files from ftp Server : " + ex.getMessage());
        }
        finally {
            try {
                if (client.isConnected()) {
                    client.logout();
                    client.disconnect();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * Odesílání binárních souborů na server.
     * Vytvoří spojení se serverem a přenese soubor (z cesty zadanou jako lokální na  sercverovou).
     * V případě chyby spojení, nebo stahování se spojení ukončí a odhlásí uživatele FTP serveru.
     */
    public void sendImageToServer(){
        FTPClient client = new FTPClient();

        try {
            client.connect(FTP_SERVER, PORT);
            client.login(USERNAME, PASSWORD);
            client.setBufferSize(2048000);

            FileInputStream fis = new FileInputStream(localFile);
            client.setFileType(FTPClient.BINARY_FILE_TYPE);
            client.storeFile(serverFile, fis);

        } catch (IOException ex) {
            System.out.println("Texture "+localFile+" sent to server.");
        }
        finally {
            try {
                if (client.isConnected()) {
                    client.logout();
                    client.disconnect();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    //Následují settery pro lokální a serverovou cestu.

    public void setServerFile(String serverFile) {
        this.serverFile = serverFile;
    }

    public void setLocalFile(String localFile) {
        this.localFile = localFile;
    }


}
