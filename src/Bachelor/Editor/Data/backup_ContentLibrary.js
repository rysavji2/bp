"use strict";

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Library for text content of the application in multiple languages.
 *
 * @author smrceant (Antonin Smrcek)
 *
 * @constructor
 */
var ContentLibrary = function()
{
 return {
 /**
 * Creates an object, keyed by internal name of the string (e.g. "CONTINUE_BUTTON_CAPTION") which holds a different
 * language translations for string that will be displayed (e.g. "Continue", "Pokracovat").
 *
 * IMPORTANT: translations for languages must be passed into the function in the same order as they are defined in
 * the Languages enum, i.e. if EN = 0, CZ = 1, then we should call the function for example like this:
 *
 * createContentString ("CONTINUE_BUTTON_CAPTION", "Continue", "Pokracovat").
 *
 * You can also copy existing string like this:
 * createContentString ("CAPTION_ASTLE", "PLACE_CASTLE").
 *
 * Both strings will have same translations now.
 *
 * @param {string} name internal name of the content string (used in the source code)
 * @param {string} [localizedStringsOrContentString] string written in supported languages or content string to copy
 */
 createContentString: function (name, localizedStringsOrContentString)
 {
 name = name.toUpperCase();

 // only 2 arguments = name of the content string and name of the content string which will be copied into it
 if (arguments.length === 2)
 {
 if (Util.isInitialized(this[ arguments[1] ]))
 {
 this[name] = this [ arguments[1] ];
 }
 }
 // 3 and more arguments = name of the string + provided translations
 else
 {
 this[name] = {};

 // remaining arguments (from 1 to N) are localized strings
 for (var i = 1; i < arguments.length; i++)
 {
 // (i-1, since acronyms for languages start from 0)
 this[name][i-1] = arguments[i];
 }
 }
 },

 /**
 * Just like createContentString(), but this method explicitly defines for which language we
 * are adding the translation.
 *
 * @param {number} language for which language we specify translation
 * @param {string} name internal name of the content string (used in the source code)
 * @param {string} localizedString string written in specified language given by the parameter
 */
 createContentStringInLanguage: function (language, name, localizedString)
 {
 name = name.toUpperCase().replace('__', '_');

 if (!Util.isInitialized (this[name]))
 {
 this[name] = {};
 }

 this[name][language] = localizedString;
 },

 /**
 * Gets a translation of the string for the language which is currently turned on.
 *
 * @param {string} name internal name of the content string
 * @returns {string} string in the current language of the application, which should be displayed
 */
 getContentString: function (name)
 {
 name = name.toUpperCase().replace('__', '_');

 if (!Util.isInitialized (this[name]))
 {
 return false;
 }

 return this[name][Preferences.Languages.getCurrentLanguage()];
 },

 /**
 * [DEV] Checks that content string exists.
 *
 * @param {string} name internal name of the content string
 * @returns {boolean} TRUE if content string with such name exists
 */
 existsContentString: function (name)
 {
 name = name.toUpperCase();

 return Util.isInitialized(this[name]);
 },

 /**
 * Splits content string text into multiple content string objects (split by newline character).
 * Example:
 * Content string with name "CONTROL_TEXT_ALTITUDE", one of its translations is "Fly altitude\nchange".
 * Then we will create 2 new content string objects.
 * First will have name "CONTROL_TEXT_ALTITUDE_0", one of its translations will be "Fly altitude".
 * Second will have name "CONTROL_TEXT_ALTITUDE_1", one of its translations will be "change".
 *
 * This method is used very rarely. It's a "hack" for the SVG images translation, where each line of the text
 * is new element. Thus we cannot use just one content object for translation, we must have content object for
 * each line.
 *
 * @param {string} name internal name of the content string (used in the source code)
 */
 splitContentStringByNewlines: function (name)
 {
 name = name.toUpperCase();

 var languageCount = Preferences.Languages.getLanguageCount();
 for (var language = 0; language < languageCount; language++)
 {
 var splittedText = this[name][language].split("\n");

 for (var textPart = 0; textPart < splittedText.length; textPart++)
 {
 ContentLibrary.createContentStringInLanguage (language, name + "_" + textPart, splittedText[textPart]);
 }
 }
 }
 };
}();

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// STRINGS
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
ContentLibrary.createContentString("EDGE_BROWSER", "It seems you're using Microsoft Edge web browser. \nThis browser is not fully supported yet and application can exhibit some issues. \nWe recommend using Google Chrome or Mozilla Firefox browsers for better experience.",
	"Vypadá to, že použí­váte prohlí­žeč Microsoft Edge. \nTento prohlí­žeč není­ aplikací­ zatí­m plně podporován, aplikace se může mí­sty chovat problémově. \nPro lepší­ zážitek doporučujeme použí­t prohlí­žeče Google Chrome nebo Mozilla Firefox.");

ContentLibrary.createContentString("EDGE_BROWSER_CONTROLS", "It seems you're using Microsoft Edge web browser. \nThis browser can have problems with key presses in 3D applications. \nPlease use buttons in the user interface if keyboard control doesn't work.",
	"Vypadá to, že použí­váte prohlí­žeč Microsoft Edge. \nTento prohlí­žeč může mí­t problémy s ovládání­m pomocí­ klávesnice ve 3D aplikací­ch. \nPokud ovládání­ klávesnicí­ nefunguje, použijte prosí­m tlačí­tka v uživatelském rozhraní­.");

ContentLibrary.createContentString("CONTINUE_BUTTON_CAPTION", "Continue", "Pokračovat");

ContentLibrary.createContentString("QUALITY_OPTION_HIGH", "High", "Vysoká");
ContentLibrary.createContentString("QUALITY_OPTION_MEDIUM", "Medium", "Střední");
ContentLibrary.createContentString("QUALITY_OPTION_LOW", "Low", "Nízká");

ContentLibrary.createContentString("QUALITY_OPTION_ON", "On", "Zap.");
ContentLibrary.createContentString("QUALITY_OPTION_OFF", "Off", "Vyp.");


ContentLibrary.createContentString("QUALITY_OPTION_SLOW", "slow", "pomalá");
ContentLibrary.createContentString("QUALITY_OPTION_FAST", "fast", "rychlá");

ContentLibrary.createContentString("OPTION_RESOLUTION", "Resolution", "Rozlišení");
ContentLibrary.createContentString("OPTION_RESOLUTION_INFO",
 "This is a resolution in which the application is physically rendered. Higher resolution means better visual quality, but also bigger demand for performance. By choosing resolution bigger than 1x, Super-Sampling Anti-Aliasing is performed by the browser. By choosing resolution smaller than 1x, you save some performance, but the price is a blurry image.",
 "Toto je fyzické rozlišení, ve kterém je aplikace vykreslována. Vyšší rozlišení znamená lepší vizuální kvalitu, ale potřebuje také vyšší výpočetní výkon. Volbou rozlišení většího než 1x se dosáhne v praxi toho, že prohlížeč bude provádět Super-Sampling Anti-Aliasing. Volbou nižšího rozlišení než 1x se ušetří výkon za cenu rozmazanějšího obrazu.");

ContentLibrary.createContentString("OPTION_FULLSCREEN", "Fullscreen", "Celá obrazovka");
ContentLibrary.createContentString("OPTION_FULLSCREEN_INFO",
 "Fullscreen means that only the application is visible on the screen. Recommended for the best experience.",
 "Celá obrazovka je mód, kdy je na obrazovce vidět pouze obsah aplikace. Pro nejlepší zážitek z aplikace je doporučeno tuto volbu zapnout.");

ContentLibrary.createContentString("OPTION_INFO_CUBES", "Info cubes", "Informační­ kostky");
ContentLibrary.createContentString("OPTION_INFO_CUBES_INFO",
 "Turns on/off the rotating informational cubes which signalize objects of interest. ",
 "Zapne či vypne rotující­ informační­ kostky, které signalizují zajímavé objekty.");

 ContentLibrary.createContentString("OPTION_COLLISION_DETECTION", "Collisions", "Kolize");
 ContentLibrary.createContentString("OPTION_COLLISION_DETECTION_INFO",
  "Turns the collision detection on/off. ",
  "Zapne či vypne detekci kolizí.");

	ContentLibrary.createContentString("OPTION_MULTIPLAYER", "Multiplayer", "Multiplayer");
	ContentLibrary.createContentString("OPTION_MULTIPLAYER_INFO",
	 "Turns the other users visibility on/off. ",
	 "Zapne nebo vypne zobrazení ostatních uživatelů.");

ContentLibrary.createContentString("OPTION_MOVING_SPEED", "Moving speed", "Rychlost pohybu");
ContentLibrary.createContentString("OPTION_MOVING_SPEED_INFO",
 "Changes how fast you move. ",
 "Mění rychlost vašeho pohybu.");

ContentLibrary.createContentString("OPTION_MOUSE_SENSITIVITY", "Mouse speed", "Rychlost myši");
ContentLibrary.createContentString("OPTION_MOUSE_SENSITIVITY_INFO",
 "Determines how fast the view changes when looking around with the mouse.",
 "Určuje jak rychle se mění pohled při rozhlížení pomocí myši.");

ContentLibrary.createContentString("OPTION_MOUSE_AXIS_INVERT", "Invert mouse Y", "Invertovat osu Y");
ContentLibrary.createContentString("OPTION_MOUSE_AXIS_INVERT_INFO",
 "Inverts the vertical axis of the mouse.",
 "Invertuje svislou osu myši.");

ContentLibrary.createContentString("OPTION_STRAFING", "Strafing", "Úkroky");
ContentLibrary.createContentString("OPTION_STRAFING_INFO",
 "Turns on/off strafing with keyboard left and right arrows",
 "Zapne či vypne úkroky pomocí levé a pravé šipky na klávesnici.");

ContentLibrary.createContentString("OPTION_MUSIC", "Music", "Hudba");
ContentLibrary.createContentString("OPTION_MUSIC_INFO",
 "Turns the background music on/off.", "Zapíná či vypíná hudbu na pozadí.");

ContentLibrary.createContentString("OPTION_SSAO", "SSAO", "SSAO");
ContentLibrary.createContentString("OPTION_SSAO_INFO",
 "Turns the Screen Space Ambient Occlusion on/off. It is a global illumination technique that emphasizes tiny details on surfaces and adds soft shadows. However, it may be heavy on the performance. Turn it off if you have low FPS.",
 "Zapne či vypne techniku Screen Space Ambient Occlusion, což je technika globálního osvětlení která zdůrazňuje malé detaily na povrchu a přidává měkké stíny. Bohužel však může být náročná na výkon. Pro urychlení aplikace volbu vypněte.");

ContentLibrary.createContentString("OPTION_FXAA", "FXAA", "FXAA");
ContentLibrary.createContentString("OPTION_FXAA_INFO",
 "Turns the Fast Approximate Anti-Aliasing on/off. Anti-Aliasing smooths jagged lines of models. Although this algorithm shall be fast, it may still be a performance burden. Turn it off if you have low FPS.",
 "Zapne či vypne Fast Approximate Anti-Aliasing. Anti-Aliasing vyhlazuje zubaté hrany modelů. Přestože by tento algoritmus měl běžet rychle, může stále představovat výkonnostní zátěž. Pro urychlení aplikace volbu vypněte.");

ContentLibrary.createContentString("OPTION_LENS_FLARE", "Lens Flare", "Lens Flare");
ContentLibrary.createContentString("OPTION_LENS_FLARE_INFO",
 "Turns the Lens Flare effect on/off. The effect simulates the phenomenon of looking at a bright light (e.g. the sun) through the lens of a camera and accentuates the light's brightness. It may be heavy on the performance, you should definitely turn it off if you have low FPS.",
 "Zapne či vypne Lens Flare efekt. Efekt simuluje jev pozorování jasného zdroje světla (např. slunce) přes čočku kamery a zdůrazňuje světelnost zdroje. Efekt může být náročný na výkon, určitě ho vypněte, pokud si přejete aby aplikace běžela rychleji.");

ContentLibrary.createContentString("OPTION_SHADOWS", "Shadows", "Stíny");
ContentLibrary.createContentString("OPTION_SHADOWS_INFO",
 "Turns the computation of shadows casted by the sun on/off.",
 "Zapne či vypne počítání stínů vržených sluncem.");

ContentLibrary.createContentString("OPTION_NIGHT_LIGHTS", "Night lights", "Noční světla");
ContentLibrary.createContentString("OPTION_NIGHT_LIGHTS_INFO",
 "Turns point and spot lights in the castle area which shine in the night.",
 "Zapne či vypne bodové a reflektorové zdroje světla která září v noci v areálu hradu.");

ContentLibrary.createContentString("OPTION_TIME_SIMULATION", "Time simulation", "Simulace času");
ContentLibrary.createContentString("OPTION_TIME_SIMULATION_INFO",
 "Turns the time simulation on/off. By turning it off, you can stop the time. The sun will not move when the time is stopped.",
 "Zapne či vypne bodové simulaci času. Vypnutím simulace lze zastavit čas. Při zastaveném čase se slunce nebude pohybovat.");

ContentLibrary.createContentString("OPTION_NORMAL_BUMP_MAPPING", "Normal/Bump mapping", "Normal/Bump mapping");
ContentLibrary.createContentString("OPTION_NORMAL_BUMP_MAPPING_INFO",
 "Turns the normal and bump mapping techniques on/off. The techniques are used to improve the appearance of some surfaces using a simple illumination trick.",
 "Zapne či vypne bodové techniku normal/bump mappingu, která je v aplikaci využita ke zlepšení vzhledu některých povrchů užitím jednoduchého osvětlovacího triku.");

ContentLibrary.createContentString("OPTION_SHOW_FPS", "Show FPS", "Zobrazit FPS");
ContentLibrary.createContentString("OPTION_SHOW_FPS_INFO",
 "Turns displaying of FPS on/off. If the FPS is high, but the application is not running smooth, try to turn off vsync (please refer to <a href='https://cesiumjs.org/2014/12/01/WebGL-Profiling-Tips/' target='_blank'>this link</a> to learn how to do that).",
 "Zapne či vypne zobrazování FPS. Pokud je FPS vysoké, ale aplikace neběží plynule, zkuste vypnout vsync (laskavě odkazujeme na <a href='https://cesiumjs.org/2014/12/01/WebGL-Profiling-Tips/'>tento odkaz</a> pro návod jak toho docílit).");

ContentLibrary.createContentString("MENU_INFO_ICON_TITLE", "Click to show/hide information about the option", "Stiskněte pro zobrazení/skrytí informací o volbě");

ContentLibrary.createContentString("OPTION_DAY_NIGHT", "Time of the day", "Čas");
ContentLibrary.createContentString("OPTION_DAY_NIGHT_INFO",
 "Drag the slider to change time of the day. The time affects position of the sun and its intensity.",
 "Táhnutím za posuvník lze měnit čas. Čas ovlivňuje pozici slunce a intenzitu jeho záření.");

ContentLibrary.createContentString("DAY_NIGHT_LOW", "0:00", "0:00");
ContentLibrary.createContentString("DAY_NIGHT_HIGH", "23:59", "23:59");

ContentLibrary.createContentString("OPTION_TIME_SPEED", "Time speed", "Rychlost času");
ContentLibrary.createContentString("OPTION_TIME_SPEED_INFO",
 "Drag the slider to change application time speed.",
 "Táhnutím za posuvník lze měnit rychlost času.");

ContentLibrary.createContentString("TIME_SPEED_LOW", "1x", "1x");
ContentLibrary.createContentString("TIME_SPEED_HIGH", "1440x", "1440x");

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// MAIN MENU CAPTIONS
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

ContentLibrary.createContentString("TAB_CAPTION_GENERAL", "General", "Obecné");
ContentLibrary.createContentString("TAB_CAPTION_DISPLAY", "Display", "Zobrazení");
ContentLibrary.createContentString("TAB_CAPTION_LIGHTS", "Time" , "Čas");
ContentLibrary.createContentString("TAB_CAPTION_HELP", "Help" , "Nápověda");
ContentLibrary.createContentString("TAB_CAPTION_TOUR", "Tour" , "Procházka");
ContentLibrary.createContentString("TAB_CAPTION_CREDITS", "Credits", "Autoři");

ContentLibrary.createContentString("CLOSE", "Close", "Zavřít");
ContentLibrary.createContentString("CONTINUE", "Continue", "Pokračovat");
ContentLibrary.createContentString("RETURN_TO_VIRTUAL_WALK", "Return to the virtual walk", "Zpět k virtuální procházce");

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CREDITS
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

ContentLibrary.createContentString("FEL_CVUT", "Faculty of Electrical Engineering, Czech Technical University in Prague", "Fakulta elektrotechnická, České vysoké učení technické v Praze");
ContentLibrary.createContentString("DCGI_CVUT", "Department of Computer Graphics and Interaction", "Katedra počítačové grafiky a interakce");
ContentLibrary.createContentString("OI_CVUT", "Open Informatics", "Otevřená informatika");

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ON-SCREEN GUI
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

ContentLibrary.createContentString("GUI_AREA_CAPTION_INFO" , "Your current position:", "Právě se nacházíte:");
ContentLibrary.createContentString("GUI_ONSCREEN_MENU" , "Settings" , "Nastavení");
ContentLibrary.createContentString("GUI_ONSCREEN_HELP" , "Help" , "Nápověda");
ContentLibrary.createContentString("GUI_ONSCREEN_TOUR" , "Tour" , "Procházka");
ContentLibrary.createContentString("GUI_ONSCREEN_TOUR_PAUSE", "Pause", "Pozastavit");
ContentLibrary.createContentString("GUI_ONSCREEN_WALK" , "Walk" , "Chůze");
ContentLibrary.createContentString("GUI_ONSCREEN_FLY" , "Fly" , "Létání");
ContentLibrary.createContentString("GUI_ONSCREEN_VIEWPOINT" , "Viewpoint", "Pohledy");
ContentLibrary.createContentString("GUI_ONSCREEN_MAP" , "Map" , "Mapa");
ContentLibrary.createContentString("GUI_ONSCREEN_OBJECTS" , "Monuments", "Zajímavosti");
ContentLibrary.createContentString("GUI_ONSCREEN_EN" , "English" , "Angličtina");
ContentLibrary.createContentString("GUI_ONSCREEN_CS" , "Czech" , "Čeština");
ContentLibrary.createContentString("GUI_ONSCREEN_FULLSCREEN" , "Fullscreen" , "Fullscreen");
ContentLibrary.createContentString("GUI_ONSCREEN_GUI" , "GUI" , "GUI");

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GUI DIALOGS
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

ContentLibrary.createContentString("GUI_VIEWPOINT_SELECT_DIALOG_TITLE", "Choose viewpoint" , "Zvolte stanoviště");
ContentLibrary.createContentString("GUI_MAP_DIALOG_TITLE" , "Map of the Prague Castle", "Mapa Pražského hradu");

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// KEYBOARD CONTROLS IN THE HELP SECTION
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
ContentLibrary.createContentString("HELP_SHOW_ADVANCED_CONTROLS", "Show advanced controls", "Zobrazit pokročilé ovládání");
ContentLibrary.createContentString("HELP_SHOW_BASIC_CONTROLS", "Show basic controls", "Zobrazit základní ovládání");

ContentLibrary.createContentString("CONTROL_TEXT_WALKFLY", "Walk/Fly", "Chůze/Let");
ContentLibrary.createContentString("CONTROL_TEXT_MOVE", "Move", "Pohyb");
ContentLibrary.createContentString("CONTROL_TEXT_TURN", "Turn", "Otáčení");
ContentLibrary.createContentString("CONTROL_TEXT_MAP", "Map", "Mapa");
ContentLibrary.createContentString("CONTROL_TEXT_HELP", "Help", "Nápověda");
ContentLibrary.createContentString("CONTROL_TEXT_MENU", "Menu", "Menu");
ContentLibrary.createContentString("CONTROL_TEXT_VIEWPOINT", "Switch Viewpoint", "Změna stanoviště");

ContentLibrary.createContentString("CONTROL_TEXT_ALTITUDE", "Fly altitude\nchange", "Změna\nvýšky letu");
ContentLibrary.createContentString("CONTROL_TEXT_JUMP", "Jump", "Skok");

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// PLACES
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
ContentLibrary.createContentString("OUTSIDE" , "(outside)" , "(venku)");

ContentLibrary.createContentString("COURTYARD3", "III. Courtyard" , "III. nádvoří");
ContentLibrary.createContentString("KE_HRADU", "Ke Hradu Street" , "Ulice Ke Hradu");
ContentLibrary.createContentString("NERUDOVA", "Nerudova Street" , "Ulice Nerudova");
ContentLibrary.createContentString("THEATINSKY_KLASTER", "Klášter theatinů" , "Theatin Monastery");
ContentLibrary.createContentString("KOSTEL_PANNY_MARIE", "Kostel Panny Marie", "Church Panny Marie");
ContentLibrary.createContentString("THUNOVSKA", "Thunovská Street" , "Thunovská ulice");
ContentLibrary.createContentString("SNEMOVNI", "Sněmovní Stree" , "Sněmovní Ulice");
ContentLibrary.createContentString("PETIKOSTELNI_NAMESTI", "Pětikostlení Square" , "Pětikostlení náměstí");
ContentLibrary.createContentString("U_ZLATE_STUDNE", "U Zlaté Studně Street" , "Ulice U Zlaté studně");
ContentLibrary.createContentString("VALDSTEJNSKA", "Valdštejnská Street" , "Valdštejnská Ulice");
ContentLibrary.createContentString("VALDSTEJNSKE_NAMESTI", "Valdštejnské Square" , "Valdštejnské náměstí");
ContentLibrary.createContentString("TOMASSKA", "Tomášská Street" , "Tomášská ulice");
ContentLibrary.createContentString("VALDSTEJNSKA_ZAHRADA", "Valdštejnská Garden" , "Valdštejnská zahrada");
ContentLibrary.createContentString("VALDSTEJNSKY_PALAC", "Valdštejnsky Palace" , "Valdštejnský palác");
ContentLibrary.createContentString("UVOZ", "Úvoz Street" , "Ulice Úvoz");
ContentLibrary.createContentString("RADNICKE_SCHODY", "Radnicke Schody Street" , "Ulice Radnické Schody");
ContentLibrary.createContentString("SOCHA_TGM", "TGM Statue" , "Socha TGM");
ContentLibrary.createContentString("MARIANSKY_SLOUP", "Marian Column" , "Mariánský morový sloup");
ContentLibrary.createContentString("SALMOVSKY_PALAC", "Salmovsky Palace" , "Salmovský palác");
ContentLibrary.createContentString("SCHWARZENBERK_PALAC", "Schwarzenberg Palace" , "Schwarzenberský palác");
ContentLibrary.createContentString("BOSYCH_KARMELITEK", "Monastery Bosých Karmelitek" , "Klášter bosých karmelitek");
ContentLibrary.createContentString("ARCIBISKUPSKY_PALAC", "Arcibishop Palace" , "Arcibiskupský palác");
ContentLibrary.createContentString("STERNBERK_PALAC", "Sternberg Palace" , "Šternberský palác");
ContentLibrary.createContentString("KOHLOVA_KASNA", "Kohl's Fountain" , "Kohlova kašna");
ContentLibrary.createContentString("MRAKOTINSKY_MONOLIT", "Obelisk" , "Mrákotinský monolit");
ContentLibrary.createContentString("SOCHA_SV_JIRI", "St. Jiří Statue" , "Socha sv. Jiří");
ContentLibrary.createContentString("VIKARSKA_ULICE", "Vikářská Street" , "Vikářská ulice");
ContentLibrary.createContentString("MIHULKA", "Mihulka Tower" , "Věž Mihulka");
ContentLibrary.createContentString("KATEDRALA_SV_VITA", "St. Vitus Cathedral" , "Katerdála sv. Víta");
ContentLibrary.createContentString("STARE_PROBOSTSTVI", "Staré proboštství" , "Staré proboštství");
ContentLibrary.createContentString("PRASNY_MOST", "Prašný Bridge" , "Prašný most");
ContentLibrary.createContentString("NAMESTI_U_SV_JIRI", "U sv. Jiří Square" , "Náměstí U sv. Jiří");
ContentLibrary.createContentString("MATYASOVA_BRANA", "Matthias Gate" , "Matyášova brána");
ContentLibrary.createContentString("STARY_KRALOVSKY_PALAC", "Old Royal Palace" , "Starý královský palác");
ContentLibrary.createContentString("JIRSKA", "Jiřská Street" , "Jiřská ulice");
ContentLibrary.createContentString("NA_OPYSI", "Na Opyši Street" , "Ulice Na Opyši");
ContentLibrary.createContentString("STARE_SCHODY", "Old Castle Stairs" , "Staré zámecké schody");
ContentLibrary.createContentString("FURSTENBER_ZAHRADA", "Fürstenbers Garden" , "Fürstenberská zahrada");
ContentLibrary.createContentString("KOLOVRATSKY_PALAC", "Kolovrat Palace" , "Kolovratský palác");
ContentLibrary.createContentString("SVATOVARINECKA_VINICE", "St. Laurent vineyard" , "Svatovavřinecká vinice");
ContentLibrary.createContentString("ZAHRADA_NA_OPYSI", "Na Opyši Garden" , "Zahrada Na Opyši");
ContentLibrary.createContentString("DALIBORKA", "Daliborka Tower" , "Věž Daliborka");
ContentLibrary.createContentString("CERNA_VEZ", "Black Tower" , "Černá věž");
ContentLibrary.createContentString("LOBKOWICZKY_PALAC", "Lobkowicz Palace" , "Lobkowiczký palác");
ContentLibrary.createContentString("ROZMBERSKY_PALAC", "Rosenberg Palace" , "Rožmberský palác");
ContentLibrary.createContentString("KOSTEL_VSECH_SVATYCH", "All Saints Church" , "Kostel Všech svatých");
ContentLibrary.createContentString("KLASTER_SV_JIRI", "St. George's Convent" , "Klášter svatého Jiří");
ContentLibrary.createContentString("BAZILIKA_SV_JIRI", "St. George's Basilica" , "Bazilika sv. Jiří");
ContentLibrary.createContentString("NOVE_PROBOSTSTVI", "Nové proboštství", "Nové proboštství");
ContentLibrary.createContentString("BILA_VEZ", "White Tower", "Bílá věž");
ContentLibrary.createContentString("NEJVYSSI_PURKRABSTVI", "Nějvyšší purkrabství", "Nějvyšší purkrabství");
ContentLibrary.createContentString("RAJSKA_ZAHRADA", "Garden of Eden", "Rajská zahrada");
ContentLibrary.createContentString("NA_VALECH", "Na Valech Garden", "Zahrada Na Valech");
ContentLibrary.createContentString("KAPITULNI_KNIHOVNA", "Kapitulní knihovna", "The Chapter library");
ContentLibrary.createContentString("KASNA_NA_NAMESTI_U_SV_JIRI", "Kašna", "Fountain");
ContentLibrary.createContentString("VYCHODNI_BRANA", "Východní brána", "Fountain");
ContentLibrary.createContentString("HRADNI_BRANA", "Hlavní brána", "Main gate");
ContentLibrary.createContentString("NA_BASTE", "Na baště", "Na baště");
ContentLibrary.createContentString("USTAV_SLECHTICEN", "Ústav šlechtičen", "Ústav šlechtičen");
ContentLibrary.createContentString("KAPLE_SV_KRIZE", "Kaple sv. Kříže", "Kaple sv. Kříže");
ContentLibrary.createContentString("STUDNA", "Studna s renesanční mříží", "Well");

ContentLibrary.createContentString("ZAMECKE_SCHODY" , "Castle stairs" , "Zámecké schody");
ContentLibrary.createContentString("CATHEDRAL" , "St. Vitus' Cathedral", "Katedrála Sv. Víta");
ContentLibrary.createContentString("VICAR_STREET" , "Vicar Street" , "Vikářská");
ContentLibrary.createContentString("GEORGES_SQUARE" , "St. George Square" , "Náměstí sv. Jiří");
ContentLibrary.createContentString("GEORGES_LANE" , "St. George's Lane" , "Jiřská");
ContentLibrary.createContentString("ZLATA_ULICKA" , "Golden Lane" , "Zlatá ulička");
ContentLibrary.createContentString("HOLY_ROOD_CHAPEL" , "Chapel of the Holy Rood" , "Kaple u Sv. Kříže");

ContentLibrary.createContentString("HRADCANY_SQUARE" , "Hradčany Square" , "Hradčanské náměstí");
ContentLibrary.createContentString("COURTYARD1" , "I. Courtyard" , "I. nádvoří");
ContentLibrary.createContentString("COURTYARD2" , "II. Courtyard" , "II. nádvoří");
ContentLibrary.createContentString("COURTYARD2_ROOF1" , "II. Courtyard (roof)", "II. nádvoří (střecha)");
ContentLibrary.createContentString("COURTYARD4" , "IV. Courtyard" , "IV. nádvoří");
ContentLibrary.createContentString("GARDEN_BASTION" , "Garden on the Bastion", "Zahrada Na Baště");
ContentLibrary.createContentString("STAG_MOAT" , "Stag Moat" , "Jelenní příkop");
ContentLibrary.createContentString("CASTLE_FROM_ABOVE" , "View from sky" , "Pohled z nebe");

ContentLibrary.createContentString("KOHLS_FOUNTAIN" , "Kohl's Fountain" , "Kohlova kašna");
ContentLibrary.createContentString("GUARD_BOX" , "Guard Box", "Strážní budka");
ContentLibrary.createContentString("IMPERIAL_STABLES" , "Imperial Stables and Prague Picture Gallery", "Císařská konírna a Obrazárna Pražského hradu");
ContentLibrary.createContentString("MATTHIAS_GATE" , "Matthias Gate", "Matyášova brána");
ContentLibrary.createContentString("SPANISH_HALL" , "Spanish Hall", "Španělský sál");

ContentLibrary.createContentString("MAP_LOCATION_WINDOW_GO_TO_OBJECT" , "Go to location" , "Přejít do lokace");
ContentLibrary.createContentString("MAP_LOCATION_WINDOW_OPEN_INFO" , "Open info" , "Otevřít info");

ContentLibrary.createContentString("MAP_HELP" , "xxx en " , "xxx cz!!!");

ContentLibrary.createContentString("UNSPECIFIED", "no caption" , "žádný titulek");

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

ContentLibrary.createContentString("BIGMAP_HELP",
	'Your current position is marked by the sticky figure. You can click and drag the figure to teleport any normally accessible place in the map',
	'Vaše aktuální pozice je označena panáčkem. Po kliknutí na panáčka a jeho přetažení na libovolné přístupné místo na mapě se na toto místo přenesete'
);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

ContentLibrary.createContentString("MAP_MARKER_TELEPORT", "Click the marker and drag it to teleport anywhere instantly" , "Kliknutím a tažením panáčka se lze okamžitě kamkoliv přemístit");
ContentLibrary.createContentString("MAP_MARKER_TELEPORT_FURTHER_HELP", "Now keep the mouse button pressed and move the marker where you want to teleport" , "Držte tlačítko myši stisknuté a panáčka přetáhněte tam, kam se chcete přemístit");

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

ContentLibrary.createContentString("CANVAS_NOT_SUPPORTED",
 "Canvas element is not supported by your web browser, which means that 3D graphics cannot be displayed. We recommend downloading the latest versions of <a href='https://www.mozilla.org/en-US/firefox/products/'>Firefox</a> or <a href='http://www.google.com/chrome/'>Chrome</a> web browsers.",
 "Canvas element není Vaším webovým prohlížečem podporován, což znamená, že nemůže být zobrazena žádná 3D grafika. Doporučujeme stáhnout nejnovější verzi webových prohlížečů <a href='https://www.mozilla.org/en-US/firefox/products/'>Firefox</a> nebo <a href='http://www.google.com/chrome/'>Chrome</a>.");
