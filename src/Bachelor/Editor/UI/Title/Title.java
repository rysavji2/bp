/*
    Title.java
    Jiří Ryšavý
    11.07.2019
    Editor modelu Pražského hradu. (Bachelor Thesis)
*/
package Bachelor.Editor.UI.Title;

import Bachelor.Editor.UI.Menu.MenuView;

import javax.swing.*;
import java.awt.*;

public class Title extends JFrame {

    private JLabel linkCz, linkEn, h1Cz, h1En, imgCz, imgAltCz, imgEn, imgAltEn, textCz, textEn;
    private JTextField linksCz, linksEn, headerCz, headerEn, imageLinkCz, imageAltCz, imageLinkEn, imageAltEn;
    private  JTextArea textInCZ, textInEn;

    public Title(String title, MenuView.Language language) {
        super(title);
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        setSize(new Dimension(900, 650));
        setLayout(new BorderLayout(10, 10));
        getContentPane().setBackground(new Color(236,57,5));
        getContentPane().setForeground(Color.WHITE);

        if(language.equals(MenuView.Language.CZ)) {
            linkCz = new JLabel("Odkazy k českému textu. Oddělené [, ]:");
            linkEn = new JLabel("Odkazy k anglickému textu. Oddělené [, ]:");
            textCz = new JLabel("Popisující text v četině:");
            textEn = new JLabel("Popisující text v angličtině:");
            h1Cz = new JLabel("Nadpis v češtině:");
            h1En = new JLabel("Nadpis v angličtině:");
            imgCz = new JLabel("Odkaz na obrázek pro český text:");
            imgAltCz = new JLabel("Popis k obrázku pro český text:");
            imgEn = new JLabel("Odkaz na obrázek pro anglický text:");
            imgAltEn = new JLabel("Popis k obrázku pro angliský text:");
        }
        else {
            linkCz = new JLabel("Links for czech text. Seppared [, ]:");
            linkEn = new JLabel("Links for english text. Seppared [, ]:");
            textCz = new JLabel("Text in czech:");
            textEn = new JLabel("Text in english:");
            h1Cz = new JLabel("Headline in czech:");
            h1En = new JLabel("Headline in english:");
            imgCz = new JLabel("Link to picture for czech text:");
            imgAltCz = new JLabel("Description of picture for czech text:");
            imgEn = new JLabel("Link to picture for english text:");
            imgAltEn = new JLabel("Description of picture for english text:");
        }

        linksCz = new JTextField();
        linksEn = new JTextField();
        textInCZ = new JTextArea(5, 10);
        textInEn = new JTextArea(5, 10);
        headerCz = new JTextField();
        headerEn = new JTextField();
        imageLinkCz = new JTextField();
        imageAltCz = new JTextField();
        imageLinkEn = new JTextField();
        imageAltEn = new JTextField();

        this.add(new JLabel("   "), BorderLayout.EAST);
        this.add(new JLabel("   "), BorderLayout.SOUTH);
        this.add(new JLabel("   "), BorderLayout.WEST);
        this.add(new JLabel("   "), BorderLayout.NORTH);
        this.add(informationCube(), BorderLayout.CENTER);

    }

    private JPanel duo(JLabel header, JComponent a, boolean area){
        JPanel ret = new JPanel(new GridLayout(2,1,0,2));
        ret.setBackground(new Color(236,57,5));
        header.setForeground(Color.WHITE);

        ret.add(header);

        if(area){
            JScrollPane sp = new JScrollPane(a);
            sp.setPreferredSize(new Dimension(30, 60));
            sp.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
            ret.add(sp);
        }
        else {
            a.setPreferredSize(new Dimension(30, 10));
            ret.add(a);
        }

        return ret;
    }

    private JPanel addColumns(){
        JPanel ret = new JPanel(new GridLayout(2,2,10,2));
        ret.setBackground(new Color(236,57,5));
        ret.setForeground(Color.WHITE);
        ret.add(duo(imgCz, imageLinkCz, false));
        ret.add(duo(imgAltCz, imageAltCz, false));
        ret.add(duo(imgEn, imageLinkEn, false));
        ret.add(duo(imgAltEn, imageAltEn, false));

        return ret;
    }

    private JPanel informationCube(){
        JPanel ret = new JPanel();
        ret.setLayout(new BoxLayout(ret, BoxLayout.PAGE_AXIS));
        ret.setBackground(new Color(236,57,5));
        ret.setForeground(Color.WHITE);

        ret.add(addColumns());
        ret.add(duo(h1Cz,headerCz, false));
        ret.add(duo(textCz, textInCZ, true));
        ret.add(duo(h1En, headerEn, false));
        ret.add(duo(textEn, textInEn, true));
        ret.add(duo(linkCz, linksCz, false));
        ret.add(duo(linkEn, linksEn, false));

        return ret;
    }

    public JTextField getLinksCz() {
        return linksCz;
    }

    public JTextField getLinksEn() {
        return linksEn;
    }

    public JTextField getHeaderCz() {
        return headerCz;
    }

    public JTextField getHeaderEn() {
        return headerEn;
    }

    public JTextArea getTextInCZ() {
        return textInCZ;
    }

    public JTextArea getTextInEn() {
        return textInEn;
    }

    public JTextField getImageLinkCz() {
        return imageLinkCz;
    }

    public JTextField getImageAltCz() {
        return imageAltCz;
    }

    public JTextField getImageLinkEn() {
        return imageLinkEn;
    }

    public JTextField getImageAltEn() {
        return imageAltEn;
    }

    public void setLinksCz(String linksCz) {
        this.linksCz.setText(linksCz);
    }

    public void setLinksEn(String linksEn) {
        this.linksEn.setText(linksEn);
    }

    public void setHeaderCz(String headerCz) {
        this.headerCz.setText(headerCz);
    }

    public void setHeaderEn(String headerEn) {
        this.headerEn.setText(headerEn);
    }

    public void setTextInCZ(String textInCZ) {
        this.textInCZ.setText(textInCZ);
    }

    public void setTextInEn(String textInEn) {
        this.textInEn.setText(textInEn);
    }

    public void setImageLinkCz(String imageLinkCz) {
        this.imageLinkCz.setText(imageLinkCz);
    }

    public void setImageAltCz(String imageAltCz) {
        this.imageAltCz.setText(imageAltCz);
    }

    public void setImageLinkEn(String imageLinkEn) {
        this.imageLinkEn.setText(imageLinkEn);
    }

    public void setImageAltEn(String imageAltEn) {
        this.imageAltEn.setText(imageAltEn);
    }
}
