/*
    SelectionView.java
    Jiří Ryšavý
    11.07.2019
    Editor modelu Pražského hradu. (Bachelor Thesis)
*/
package Bachelor.Editor.UI.Selection;

import Bachelor.Editor.Manipulation.Dae.IDData;
import Bachelor.Editor.Manipulation.Dae.Solver;
import Bachelor.Editor.Manipulation.Html.CreateHtmlInfo;
import Bachelor.Editor.UI.Menu.MenuView.Language;
import Bachelor.Editor.UI.Title.Title;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.io.*;

/**
 * Třída reprezentující okno editoru.
 */
public class SelectionView extends JFrame {

    private Language language;
    private JList objects;
    private JLabel objectsHeader, daeHeader, nameHeader, X, Y, Z, Rot, Tra, Sca, Way;
    private JTextField daeText, nameText, Rx, Ry, Rz, Tx, Ty, Tz, Sx, Sy, Sz, pathX, pathY, pathZ;
    private JButton submit, browse;
    private ButtonGroup buttons;
    private JRadioButton replace, add, remove;
    private JCheckBox text;
    private Solver solver;
    private Title infoCube;
    private String filename = "";

    public SelectionView(String title, Language lang) {
        super(title);
        this.language = lang;
        setSize(new Dimension(750, 450));
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setLayout(new BorderLayout(10,10));
        setResizable(false);
        getContentPane().setBackground(new Color(236,57,5));
        getContentPane().setForeground(Color.WHITE);
        solver = new Solver();

        //Popisující texty v češtině
        if(language.equals(Language.CZ)) {
            replace = new JRadioButton("zaměnit/upravit");
            remove = new JRadioButton("odstranit");
            add = new JRadioButton("přidat", true);
            text = new JCheckBox("Přidat popis");
            submit = new JButton("Odeslat");
            browse = new JButton("Prohledat");
            infoCube = new Title("Editace popisků", Language.CZ);
        }
        //popisující texty v angličtině
        else {
            replace = new JRadioButton("replace/change");
            remove = new JRadioButton("remove");
            add = new JRadioButton("add", true);
            submit = new JButton("Submit");
            text = new JCheckBox("Add description");
            browse = new JButton("Browse");
            infoCube = new Title("Information management", Language.EN);
        }

        //Akce po kliknutí na radiobutton s možností změnit
        replace.addActionListener(e -> {
            //pokud máme zvolený objekt, zapíšeme hodnoty do příslušných polí
            if(objects.getSelectedIndex() != -1){
                IDData d = solver.getObjects().get(objects.getSelectedIndex());
                Rx.setText(Double.toString(d.getRotation()[0]));
                Ry.setText(Double.toString(d.getRotation()[1]));
                Rz.setText(Double.toString(d.getRotation()[2]));
                Tx.setText(Double.toString(d.getTranslate()[0]));
                Ty.setText(Double.toString(d.getTranslate()[1]));
                Tz.setText(Double.toString(d.getTranslate()[2]));
                Sx.setText(Double.toString(d.getScale()[0]));
                Sy.setText(Double.toString(d.getScale()[1]));
                Sz.setText(Double.toString(d.getScale()[2]));
                pathX.setText(d.getPath()[0]);
                pathY.setText(d.getPath()[1]);
                pathZ.setText(d.getPath()[2]);
                nameText.setText(d.getName());
            }
            pathZ.setEnabled(true);
            pathY.setEnabled(true);
            pathX.setEnabled(true);
            Sz.setEnabled(true);
            Sy.setEnabled(true);
            Sx.setEnabled(true);
            Tz.setEnabled(true);
            Ty.setEnabled(true);
            Tx.setEnabled(true);
            Rz.setEnabled(true);
            Rx.setEnabled(true);
            Ry.setEnabled(true);
            nameText.setEnabled(false);
            daeText.setEnabled(true);
            browse.setEnabled(true);
            text.setEnabled(true);
        });

        remove.addActionListener(e -> {
            if(objects.getSelectedIndex() != -1){
                IDData d = solver.getObjects().get(objects.getSelectedIndex());
                Rx.setText(Double.toString(d.getRotation()[0]));
                Ry.setText(Double.toString(d.getRotation()[1]));
                Rz.setText(Double.toString(d.getRotation()[2]));
                Tx.setText(Double.toString(d.getTranslate()[0]));
                Ty.setText(Double.toString(d.getTranslate()[1]));
                Tz.setText(Double.toString(d.getTranslate()[2]));
                Sx.setText(Double.toString(d.getScale()[0]));
                Sy.setText(Double.toString(d.getScale()[1]));
                Sz.setText(Double.toString(d.getScale()[2]));
                pathX.setText(d.getPath()[0]);
                pathY.setText(d.getPath()[1]);
                pathZ.setText(d.getPath()[2]);
                nameText.setText(d.getName());
            }
            pathZ.setEnabled(false);
            pathY.setEnabled(false);
            pathX.setEnabled(false);
            Sz.setEnabled(false);
            Sy.setEnabled(false);
            Sx.setEnabled(false);
            Tz.setEnabled(false);
            Ty.setEnabled(false);
            Tx.setEnabled(false);
            Rz.setEnabled(false);
            Rx.setEnabled(false);
            Ry.setEnabled(false);
            nameText.setEnabled(false);
            daeText.setEnabled(false);
            browse.setEnabled(false);
            text.setEnabled(false);
        });

        //Akce po kliknutí na radiobutton s nápisem add/přidat
        add.addActionListener(e -> {
            if(objects.getSelectedIndex() != -1){
                IDData d = solver.getObjects().get(objects.getSelectedIndex());
                Rx.setText(Double.toString(d.getRotation()[0]));
                Ry.setText(Double.toString(d.getRotation()[1]));
                Rz.setText(Double.toString(d.getRotation()[2]));
                Tx.setText(Double.toString(d.getTranslate()[0]));
                Ty.setText(Double.toString(d.getTranslate()[1]));
                Tz.setText(Double.toString(d.getTranslate()[2]));
                Sx.setText(Double.toString(d.getScale()[0]));
                Sy.setText(Double.toString(d.getScale()[1]));
                Sz.setText(Double.toString(d.getScale()[2]));
                pathX.setText(d.getPath()[0]);
                pathY.setText(d.getPath()[1]);
                pathZ.setText(d.getPath()[2]);
                nameText.setText("");
            }
            pathZ.setEnabled(true);
            pathY.setEnabled(true);
            pathX.setEnabled(true);
            Sz.setEnabled(true);
            Sy.setEnabled(true);
            Sx.setEnabled(true);
            Tz.setEnabled(true);
            Ty.setEnabled(true);
            Tx.setEnabled(true);
            Rz.setEnabled(true);
            Rx.setEnabled(true);
            Ry.setEnabled(true);
            nameText.setEnabled(true);
            daeText.setEnabled(true);
            browse.setEnabled(true);
            text.setEnabled(true);
        });

        buttons = new ButtonGroup();
        buttons.add(add);
        buttons.add(replace);
        buttons.add(remove);


        JPanel pane = new JPanel(new BorderLayout(10,10));
        pane.setBackground(new Color(236,57,5));
        pane.setForeground(Color.WHITE);
        pane.add(inserts(), BorderLayout.NORTH);
        pane.add(matrixes(), BorderLayout.CENTER);
        pane.add(buttChose(), BorderLayout.SOUTH);

        JPanel panel = new JPanel(new BorderLayout(10,10));
        panel.setBackground(new Color(236,57,5));
        panel.setForeground(Color.WHITE);
        panel.add(pane, BorderLayout.CENTER);
        panel.add(leftCol(), BorderLayout.WEST);


        text.addActionListener(e ->{
            if(text.isSelected()){
                infoCube.setVisible(true);
            }
            else{
                infoCube.setVisible(false);
            }
        });

        this.add(new JLabel("   "), BorderLayout.EAST);
        this.add(new JLabel("   "), BorderLayout.SOUTH);
        this.add(new JLabel("   "), BorderLayout.WEST);
        this.add(new JLabel("   "), BorderLayout.NORTH);
        this.add(panel, BorderLayout.CENTER);

        browse.addActionListener(e-> selectFile());
        submit.addActionListener(e -> submitHandler());

        //Akce při změně objektu v meny s objekty. Přepíše transformační parametry a informace o objektu.
        objects.addListSelectionListener(e -> {
            File fil = new File("/Bachelor/Editor/Data/"+filename+".html");
            fil.delete();
            if(objects.getSelectedIndex() != -1) {
                IDData d = solver.getObjects().get(objects.getSelectedIndex());
                Rx.setText(Double.toString(d.getRotation()[0]));
                Ry.setText(Double.toString(d.getRotation()[1]));
                Rz.setText(Double.toString(d.getRotation()[2]));
                Tx.setText(Double.toString(d.getTranslate()[0]));
                Ty.setText(Double.toString(d.getTranslate()[1]));
                Tz.setText(Double.toString(d.getTranslate()[2]));
                Sx.setText(Double.toString(d.getScale()[0]));
                Sy.setText(Double.toString(d.getScale()[1]));
                Sz.setText(Double.toString(d.getScale()[2]));
                pathX.setText(d.getPath()[0]);
                pathY.setText(d.getPath()[1]);
                pathZ.setText(d.getPath()[2]);
                if(!add.isSelected())nameText.setText(d.getName());
                filename = "";
                infoCube.setHeaderCz("");
                infoCube.setHeaderEn("");
                infoCube.setImageAltCz("");
                infoCube.setImageLinkCz("");
                infoCube.setImageAltEn("");
                infoCube.setImageLinkEn("");
                infoCube.setLinksCz("");
                infoCube.setLinksEn("");
                infoCube.setTextInCZ("");
                infoCube.setTextInEn("");
                if(!d.getInformationFile().equals("")) {
                    filename = d.getInformationFile();
                    CreateHtmlInfo c = new CreateHtmlInfo(filename + ".html");
                    c.decomposeHTML();
                    infoCube.setHeaderCz(c.getHeaderCzech());
                    infoCube.setHeaderEn(c.getHeaderEnglish());
                    infoCube.setImageAltCz(c.getImageAltCzech());
                    infoCube.setImageLinkCz(c.getImageSourceCzech());
                    infoCube.setImageAltEn(c.getImageAltEnglish());
                    infoCube.setImageLinkEn(c.getImageSourceEnglish());
                    infoCube.setLinksCz(c.getLinksCzech());
                    infoCube.setLinksEn(c.getLinksEnglish());
                    infoCube.setTextInCZ(c.getTextInCzech());
                    infoCube.setTextInEn(c.getTextInEnglish());
                }
            }else{
                Rx.setText("");
                Ry.setText("");
                Rz.setText("");
                Tx.setText("");
                Ty.setText("");
                Tz.setText("");
                Sx.setText("");
                Sy.setText("");
                Sz.setText("");
                pathX.setText("");
                pathY.setText("");
                pathZ.setText("");
                nameText.setText("");
                infoCube.setHeaderCz("");
                infoCube.setHeaderEn("");
                infoCube.setImageAltCz("");
                infoCube.setImageLinkCz("");
                infoCube.setImageAltEn("");
                infoCube.setImageLinkEn("");
                infoCube.setLinksCz("");
                infoCube.setLinksEn("");
                infoCube.setTextInCZ("");
                infoCube.setTextInEn("");
                filename = "";
            }
        });
    }

    // Následují metody pro nastavení umístění komponent UI a jejich vlastnosti.
    // Za tímto oddílem jsou dvě metody pro funkce tlačítek odeslat a prohledávání file systému.

    private JPanel leftCol(){
        JPanel ret = new JPanel(new BorderLayout(10, 10));
        ret.setBackground(new Color(236,57,5));
        ret.setForeground(Color.WHITE);
        ret.setPreferredSize(new Dimension(130,5));

        if(language.equals(Language.CZ))
            objectsHeader = new JLabel("Objekty ve scéně");
        else
            objectsHeader = new JLabel("Objects in scene");

        objectsHeader.setForeground(Color.WHITE);
        objects = new JList(solver.getNames().toArray());

        JScrollPane scrollPane = new JScrollPane(objects);

        ret.add(objectsHeader, BorderLayout.NORTH);
        ret.add(scrollPane, BorderLayout.CENTER);

        return ret;
    }

    private JPanel matrixes(){
        JPanel ret = new JPanel(new GridLayout(4, 5, 5, 5));
        ret.setBackground(new Color(236,57,5));
        ret.setForeground(Color.WHITE);

        X = new JLabel("X");
        X.setForeground(Color.WHITE);
        Y = new JLabel("Y");
        Y.setForeground(Color.WHITE);
        Z = new JLabel("Z");
        Z.setForeground(Color.WHITE);

        X.setHorizontalAlignment(JLabel.CENTER);
        Y.setHorizontalAlignment(JLabel.CENTER);
        Z.setHorizontalAlignment(JLabel.CENTER);

        if(language.equals(Language.CZ)) {
            Rot = new JLabel("Rotace");
            Tra = new JLabel("Posun");
            Sca = new JLabel("Zvětšení");
            Way = new JLabel("Bod cesty");
        }
        else {
            Rot = new JLabel("Rotation");
            Tra = new JLabel("Translation");
            Sca = new JLabel("Scale");
            Way = new JLabel("Path point");
        }

        Rot.setHorizontalAlignment(JLabel.CENTER);
        Rot.setForeground(Color.WHITE);
        Tra.setHorizontalAlignment(JLabel.CENTER);
        Tra.setForeground(Color.WHITE);
        Sca.setHorizontalAlignment(JLabel.CENTER);
        Sca.setForeground(Color.WHITE);
        Way.setHorizontalAlignment(JLabel.CENTER);
        Way.setForeground(Color.WHITE);

        Rx = new JTextField();
        Rx.setText("0");
        Ry = new JTextField();
        Ry.setText("0");
        Rz = new JTextField();
        Rz.setText("0");

        Sx = new JTextField();
        Sx.setText("1");
        Sy = new JTextField();
        Sy.setText("1");
        Sz = new JTextField();
        Sz.setText("1");

        Tx = new JTextField();
        Tx.setText("0");
        Ty = new JTextField();
        Ty.setText("0");
        Tz = new JTextField();
        Tz.setText("0");

        pathX = new JTextField();
        pathX.setText("");
        pathY = new JTextField();
        pathY.setText("");
        pathZ = new JTextField();
        pathZ.setText("");

        ret.add(new JLabel(""));
        ret.add(Rot);
        ret.add(Tra);
        ret.add(Sca);
        ret.add(Way);
        ret.add(X);
        ret.add(Rx);
        ret.add(Tx);
        ret.add(Sx);
        ret.add(pathX);
        ret.add(Y);
        ret.add(Ry);
        ret.add(Ty);
        ret.add(Sy);
        ret.add(pathY);
        ret.add(Z);
        ret.add(Rz);
        ret.add(Tz);
        ret.add(Sz);
        ret.add(pathZ);

        return ret;
    }

    private JPanel inserts(){
        JPanel ret = new JPanel(new GridLayout(4,1,0,5));
        ret.setBackground(new Color(236,57,5));

        if(language.equals(Language.CZ)) {
            nameHeader = new JLabel("Název objektu");
            daeHeader = new JLabel("Soubor .dae");
        }
        else {
            nameHeader = new JLabel("Object name");
            daeHeader = new JLabel(".dae file");
        }

        nameText = new JTextField();
        nameHeader.setForeground(Color.WHITE);
        daeHeader.setForeground(Color.WHITE);

        ret.add(daeHeader);
        ret.add(fileChose());
        ret.add(nameHeader);
        ret.add(nameText);

        return ret;
    }

    private JPanel fileChose(){
        //JPanel ret = new JPanel(new GridLayout(1,2,10,0));
        JPanel ret = new JPanel(new BorderLayout(5,1));
        ret.setBackground(new Color(236,57,5));

        daeText = new JTextField();
        daeText.setMargin(new Insets(0,10,0,10));
        browse.setMargin(new Insets(5,10,5,10));

        ret.add(daeText, BorderLayout.CENTER);
        ret.add(browse, BorderLayout.EAST);

        return ret;
    }

    private JPanel buttChose(){
        JPanel ret = new JPanel(new GridLayout(2,1));
        ret.setBackground(new Color(236,57,5));

        JPanel r1 = new JPanel(new GridLayout(1, 2, 3, 0));
        r1.setBackground(new Color(236,57,5));

        JPanel pane = new JPanel(new FlowLayout());
        pane.setBackground(new Color(236,57,5));

        replace.setBackground(new Color(236,57,5));
        replace.setForeground(Color.WHITE);
        add.setBackground(new Color(236,57,5));
        add.setForeground(Color.WHITE);
        text.setBackground(new Color(236,57,5));
        text.setForeground(Color.WHITE);
        remove.setBackground(new Color(236,57,5));
        remove.setForeground(Color.WHITE);

        pane.add(add);
        pane.add(replace);
        pane.add(remove);

        r1.add(pane);
        r1.add(text);

        ret.add(r1);
        JPanel a = new JPanel(new BorderLayout(5,1));
        a.setBackground(new Color(236,57,5));
        submit.setPreferredSize(new Dimension(100,25));
        a.add(submit, BorderLayout.EAST);
        ret.add(a);

        return ret;
    }

    /**
     * Metoda pro vyvolání prohlížeče file systému. Zadanou hodnotu, pokud je, zapíše do pole s cestou.
     * Pokud zadaný soubor nemá příponu .dae je cesta neplatná.
     */
    public void selectFile() {
        JFileChooser chooser = new JFileChooser();
        daeText.setBackground(Color.WHITE);
        nameText.setBackground(Color.WHITE);
        objects.setBackground(Color.WHITE);
        chooser.addChoosableFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                if(f.isDirectory()) return true;
                return f.getName().endsWith(".dae");
            }

            @Override
            public String getDescription() {
                return "Models (*.dae)";
            }
        });

        if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION){
            File f = chooser.getSelectedFile();
            if (f.getName().endsWith(".dae")){
                daeText.setText(f.getAbsolutePath());
            }
            else {
                if(language.equals(Language.CZ))
                    JOptionPane.showMessageDialog(this, "Podporované soubory jsou pouze *.dae");
                else
                    JOptionPane.showMessageDialog(this, "Supported files are only *.dae");
            }
        }
    }

    /**
     * Metoda obsluhující odeslání formuláře. Kontroluje, zda jsou zadané všechny potřebné parametry pro jednotlivé akce.
     * Vytvoří instanci solveru a nastaví mu potřebné parametry.
     * Následně se v novém vlákně spustí solve a zavře se okno.
     */
    public void submitHandler() {
        solver.setInfoCube(infoCube);
        try {
            solver.setRx(Double.parseDouble(Rx.getText()));
            solver.setRy(Double.parseDouble(Ry.getText()));
            solver.setRz(Double.parseDouble(Rz.getText()));
            solver.setTx(Double.parseDouble(Tx.getText()));
            solver.setTy(Double.parseDouble(Ty.getText()));
            solver.setTz(Double.parseDouble(Tz.getText()));
            solver.setSx(Double.parseDouble(Sx.getText()));
            solver.setSy(Double.parseDouble(Sy.getText()));
            solver.setSz(Double.parseDouble(Sz.getText()));
            try {
                Double.parseDouble(pathX.getText());
                Double.parseDouble(pathY.getText());
                Double.parseDouble(pathZ.getText());
            }
            catch (Exception e){
                if(!pathX.getText().equals("") || !pathY.getText().equals("") || !pathZ.getText().equals("")){
                    if (language.equals(Language.CZ))
                        JOptionPane.showMessageDialog(this, "Pokud si přejete zadat bod procházky, napište jej jako double. Pokud ne, nechejte pole prázdné");
                    else
                        JOptionPane.showMessageDialog(this, "If you want to add point for tour, write it in double values. If not, leave point arguments empty.");
                }
            }
            solver.setPx(pathX.getText());
            solver.setPy(pathY.getText());
            solver.setPz(pathZ.getText());

        } catch (Exception ex) {
            ex.printStackTrace();
            if (language.equals(Language.CZ))
                JOptionPane.showMessageDialog(this, "Hodnoty v Rotaci, Posunu a Zvětšení musí být typu double.");
            else
                JOptionPane.showMessageDialog(this, "Values in Translation, Rotation and Scale must be of type double.");
        }

        if (add.isSelected()) {
            solver.setText(text.isSelected());
            if (!daeText.getText().isEmpty() && !nameText.getText().isEmpty()){
                solver.setFileToAdd(daeText.getText());
                solver.setNameToAdd(nameText.getText());

                Runnable r = () -> solver.add();
                Thread t = new Thread(r);
                t.start();
                infoCube.dispose();
                dispose();
            }
            else {
                daeText.setBackground(Color.PINK);
                nameText.setBackground(Color.PINK);
                if (language.equals(Language.CZ))
                    JOptionPane.showMessageDialog(this, "Vyplňte *.dae nebo Název objektu.");
                else
                    JOptionPane.showMessageDialog(this, "Missing *.dae or Object name.");
            }
        }
        else if (replace.isSelected()){
            solver.setText(text.isSelected());
            if (!objects.isSelectedIndex(-1)) {
                solver.setSelected(objects.getSelectedIndex());
                solver.setFileToAdd(daeText.getText());

                Runnable r = () -> solver.replace();
                Thread t = new Thread(r);
                t.start();
                infoCube.dispose();
                dispose();
            } else {
                if (language.equals(Language.CZ))
                    JOptionPane.showMessageDialog(this, "Zvolte model, který chcete pozměnit.");
                else
                    JOptionPane.showMessageDialog(this, "Select model, which you want to change.");
            }
        }
        else {
            if (!objects.isSelectedIndex(-1)) {
                int answer;
                if (language.equals(Language.CZ)) {
                    answer = JOptionPane.showOptionDialog(this, "Opravdu chceš odstranit objekt: " + nameText.getText() + ".", "Odstranit?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, new String[] {"Ano", "Ne"}, "Ne");
                } else {
                    answer = JOptionPane.showOptionDialog(this, "Are you sure with remove of: " + nameText.getText() + ".", "Remove?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, new String[] {"Yes", "No"}, "No");
                }
                if ( answer == 0){
                    solver.setSelected(objects.getSelectedIndex());
                    Runnable r = () -> solver.remove();
                    Thread t = new Thread(r);
                    t.start();
                    infoCube.dispose();
                    dispose();
                }
            } else {
                if (language.equals(Language.CZ))
                    JOptionPane.showMessageDialog(this, "Není zvolený soubor k odstranění.");
                else
                    JOptionPane.showMessageDialog(this, "Object for remove is not selected.");
            }
        }
    }
}
