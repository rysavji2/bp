/*
    OpenApp.java
    Jiří Ryšavý
    11.07.2019
    Editor modelu Pražského hradu. (Bachelor Thesis)
*/
package Bachelor.Editor.UI.Menu;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Třída pro obsluhu tlačítka otevření aplikace.
 */
public class OpenApp implements ActionListener {

    /**
     * Event handler pro kliknutí tlačítka.
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (Desktop.isDesktopSupported()) {
            try {
                //otevře defaultní prohlížeč systému na zadané adrese
                Desktop.getDesktop().browse(new URI("http://127.0.0.1"));
                //ukončí aplikaci
                //System.exit(0);
            } catch (IOException | URISyntaxException e1) {
                System.err.println("Unexpected exception!");
                e1.printStackTrace();
            }
        }
    }
}
