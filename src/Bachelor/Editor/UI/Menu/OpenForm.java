/*
    OpenForm.java
    Jiří Ryšavý
    11.07.2019
    Editor modelu Pražského hradu. (Bachelor Thesis)
*/
package Bachelor.Editor.UI.Menu;

import Bachelor.Editor.Conection.ServerConection;
import Bachelor.Editor.UI.Selection.SelectionView;
import com.sun.jndi.toolkit.url.Uri;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Třída pro obsluhu tlačítka na otevření editoru.
 */
public class OpenForm implements ActionListener {
    private MenuView v;
    private static final String IDS_SERVER = "/data/scene/ids.info";
    private static final String IDS_LOCAL = "ids.info";
    private static final String CASTLE_SERVER = "/data/scene/prague_castle.dae";
    private static final String CASTLE_LOCAL = "castle.dae";
    private static final String TOUR_LOCAL = "tour.json";
    private static final String TOUR = "/data/tour/tour.json";
    private static final String CONTENTLIB = "src/strings/ContentLibrary.js";
    private static final String CONTENTLIB_LOCAL = "ContentLib.js";

    /**
     * Konstruktor třídy.
     * @param v menu, ze kterého je událost vyvolána
     */
    public OpenForm(MenuView v) {
        this.v = v;
    }

    /**
     * Event handler pro tlačítko. Stáhne ze serveru potřebné soubory a následně otevře okno s editorem.
     * @param e odesílatel události
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        final String title;
        if(v.getLang().equals(MenuView.Language.CZ)) title = "Editor modelů";
        else title = "Models editor";

        String property = "java.io.tmpdir";
        String tempDir = System.getProperty(property);

        //stažení modelu hradu
        ServerConection sc = new ServerConection(CASTLE_SERVER, tempDir + CASTLE_LOCAL);
        sc.downloadFromServer();

        //stažení ids.info
        sc.setLocalFile(tempDir + IDS_LOCAL);
        sc.setServerFile(IDS_SERVER);
        sc.downloadFromServer();

        //stažení json souboru s grafem
        sc.setLocalFile(tempDir + TOUR_LOCAL);
        sc.setServerFile(TOUR);
        sc.downloadFromServer();

        //stažení content library
        sc.setServerFile(CONTENTLIB);
        sc.setLocalFile(tempDir + CONTENTLIB_LOCAL);
        sc.downloadFromServer();

        //otevření okna s nastaveným jazykem na jazyk v menu
        SwingUtilities.invokeLater(() -> {
            SelectionView sv = new SelectionView(title, v.getLang());
            sv.setVisible(true);
        });
    }
}
