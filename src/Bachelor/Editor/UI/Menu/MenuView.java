/*
    MenuView.java
    Jiří Ryšavý
    11.07.2019
    Editor modelu Pražského hradu. (Bachelor Thesis)
*/
package Bachelor.Editor.UI.Menu;

import javax.swing.*;
import java.awt.*;

/**
 * Třída reprezentující okno hlavního menu.
 */
public class MenuView extends JFrame {
    private JButton openForm, openBrows;
    private JLabel title, creator, language;

    public enum Language{CZ, EN}

    private Language lang;
    private static final String ICON = "/Bachelor/Editor/Data/CastleIcon.png";

    /**
     * Vytvoření okna a jeho zobrazení.
     */
    public MenuView(){
        super("Prague castle");
        this.setSize(500,400);
        this.setResizable(false);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        JPanel pane = new JPanel(new BorderLayout());
        JPanel butt = new JPanel(new FlowLayout());
        JPanel bot = new JPanel(new BorderLayout());
        butt.setBackground(new Color(236,57,5));
        bot.setBackground(new Color(236,57,5));
        pane.setBackground(new Color(236,57,5));

        lang = Language.CZ;

        openBrows = new JButton("");
        openBrows.setPreferredSize(new Dimension(140,30));
        openForm = new JButton("");
        openForm.setPreferredSize(new Dimension(140,30));

        ImageIcon tit = new ImageIcon(this.getClass().getResource(ICON));
        title = new JLabel(tit);
        creator = new JLabel("Jiří Ryšavý & Jan Husák & Antonín Smrček");
        creator.setForeground(Color.WHITE);
        language = new JLabel("");
        language.setForeground(Color.WHITE);
        language.addMouseListener(new ChangeLanguage(this));

        butt.add(openBrows);
        butt.add(openForm);

        bot.add(creator, BorderLayout.WEST);
        bot.add(language, BorderLayout.EAST);

        pane.add(butt,BorderLayout.CENTER);
        pane.add(title, BorderLayout.NORTH);
        pane.add(bot, BorderLayout.SOUTH);

        this.add(pane);

        checkLanguage();

        openBrows.addActionListener(new OpenApp());
        openForm.addActionListener(new OpenForm(this));

    }

    /**
     * Změna jazyka okna.
     */
    void changeLang(){
        lang = lang == Language.EN ? Language.CZ : Language.EN;
        checkLanguage();
    }

    /**
     * Upraví nápisy podle právě nastaveného jazyka.
     */
    private void checkLanguage(){
        switch(lang){
            case CZ:
                openBrows.setText("Otevřít aplikaci");
                openForm.setText("Upravit modely");
                language.setText("CZ");
                break;
            case EN:
                openBrows.setText("Open application");
                openForm.setText("Edit models");
                language.setText("EN");
                break;
        }
    }
    /**
     * Getter pro aktuální jazyk
    */
    public Language getLang() {
        return lang;
    }
}
