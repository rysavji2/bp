/*
    ChangeLanguage.java
    Jiří Ryšavý
    11.07.2019
    Editor modelu Pražského hradu. (Bachelor Thesis)
*/
package Bachelor.Editor.UI.Menu;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Třída pro obsluhu změny jazyka.
 */
public class ChangeLanguage implements MouseListener {
    private MenuView mv;

    /**
     * Konstruktor třídy.
     * @param mv okno, ze kterého se bude volat.
     */
    ChangeLanguage(MenuView mv) {
        this.mv = mv;
    }

    /**
     * Při kliknití myší na objekt se změní jazyk v okně
     * @param e objekt, na který se kliklo
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        mv.changeLang();
    }

    //Nutné pro implementaci, nicméně v našem případě nepoužíváme. Proto jsou následující metody prázdné.
    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
}
