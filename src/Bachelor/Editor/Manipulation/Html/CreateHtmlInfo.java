/*
    CreateHtmlInfo.java
    Jiří Ryšavý
    11.07.2019
    Editor modelu Pražského hradu. (Bachelor Thesis)
*/
package Bachelor.Editor.Manipulation.Html;

import Bachelor.Editor.Conection.ServerConection;

import java.io.*;
import java.nio.BufferOverflowException;

/**
 * Třída pro vytváření souborů HTML pro popisky dle vzoru od Jana Husáka
 */
public class CreateHtmlInfo {
    private String textInEnglish;
    private String textInCzech;
    private String linksEnglish;
    private String linksCzech;
    private String headerEnglish;
    private String headerCzech;
    private String filename;
    private String imageSourceCzech;
    private String imageAltCzech;
    private String imageSourceEnglish;
    private String imageAltEnglish;

    /**
     * Konstruktor třídy.
     * @param textInEnglish popisující text v angličtině
     * @param textInCzech popisující text v češtině
     * @param linksEnglish odkazy pro anglický jazyk
     * @param linksCzech odkazy pro český jazyk
     * @param headerEnglish nadpis v angličtině
     * @param headerCzech nadpis v češtině
     * @param filename název výsledného souboru
     * @param imageSourceCzech zdroj pro obrázek českého textu
     * @param imageAltCzech alternativa k obrázku českého textu
     * @param imageSourceEnglish zdroj pro obrázek anglického textu
     * @param imageAltEnglish alternativa k obrázku anglického textu
     */
    public CreateHtmlInfo(String textInEnglish, String textInCzech, String linksEnglish, String linksCzech,
                          String headerEnglish, String headerCzech, String filename, String imageSourceCzech,
                          String imageAltCzech, String imageSourceEnglish, String imageAltEnglish) {
        this.textInEnglish = textInEnglish;
        this.textInCzech = textInCzech;
        this.linksEnglish = linksEnglish;
        this.linksCzech = linksCzech;
        this.headerEnglish = headerEnglish;
        this.headerCzech = headerCzech;
        this.filename = filename;
        this.imageSourceCzech = imageSourceCzech;
        this.imageAltCzech = imageAltCzech;
        this.imageSourceEnglish = imageSourceEnglish;
        this.imageAltEnglish = imageAltEnglish;
    }

    /**
     * Nastavení názvu souboru (setter)
     * @param filename nový název souboru
     */
    public CreateHtmlInfo(String filename) {
        this.filename = filename;
    }

    private static final String CONTENTLIB = "ContentLib.js";

    /**
     * Vytvoří HTML soubor dle dostupných informací a následně jej odešle.
     */
    public void createHTML(){
        String property = "java.io.tmpdir";
        String tempDir = System.getProperty(property);
        FileWriter fw;

        try {
            fw = new FileWriter(tempDir + filename + ".html");

            //english part
            fw.write("<div langtype=\"EN\">\n");
            fw.write("    <article class=\"object-info\">\n");
            fw.write("        <h1>" + headerEnglish + "</h1>\n");
            fw.write("        <img src=\"" + imageSourceEnglish + "\" alt=\"" + imageAltEnglish + "\">\n");
            fw.write("        <div class=\"content\">\n"  + textInEnglish + "\n        </div>\n");
            fw.write("        <ul class=\"source\">\n");

            String[] links = linksEnglish.split(", ");
            for (int i = 0; i < links.length; i++){
                fw.write("            <li><a target=\"_blank\" href=\"" + links[i] + "\"> link" + i + "</a></li>\n");
            }

            fw.write("        </ul>\n");
            fw.write("    </article>\n");
            fw.write("</div>\n\n");

            //czech part
            fw.write("<div langtype=\"CZ\">\n");
            fw.write("    <article class=\"object-info\">\n");
            fw.write("        <h1>" + headerCzech + "</h1>\n");
            fw.write("        <img src=\"" + imageSourceCzech + "\" alt=\"" + imageAltCzech + "\">\n");
            fw.write("        <div class=\"content\">\n"  + textInCzech + "\n        </div>\n");
            fw.write("        <ul class=\"source\">\n");

            links = linksCzech.split(", ");
            for (int i = 0; i < links.length; i++){
                fw.write("            <li><a target=\"_blank\" href=\"" + links[i] + "\"> odkaz" + i + "</a></li>\n");
            }

            fw.write("        </ul>\n");
            fw.write("    </article>\n");
            fw.write("</div>\n\n");

            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ServerConection sc = new ServerConection("/info/" + filename + ".html", tempDir + filename + ".html");
        sc.sendToServer();

        File f = new File(tempDir + filename + ".html");
        f.delete();
    }

    /**
     * Informační soubor je k ničemu, pokud soubor není zaznamenán v content library webové aplikace, kde je svázán s objektem ve scéně.
     * Při odstranění objektu tedy odstraníme i příslušný záznam v content library.
     */
    public void removeFromContent(){
        String property = "java.io.tmpdir";
        String tempDir = System.getProperty(property);

        try {
            FileWriter fw = new FileWriter(tempDir + "cl.js");
            BufferedReader reader = new BufferedReader(new FileReader(tempDir + CONTENTLIB));

            String line = reader.readLine();
            String upp = filename.toUpperCase();
            while (line != null){
                if(!line.contains(upp))
                    fw.append(line + "\n");

                line = reader.readLine();
            }

            reader.close();
            fw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        File fil = new File(tempDir + CONTENTLIB);
        fil.delete();
        File fl = new File(tempDir + "cl.js");

        fil = new File(tempDir + CONTENTLIB);
        fl.renameTo(fil);
    }

    /**
     * Vytváří záznam v content library, aby byl soubor svázán s objektem ve scéně.
     */
    public void createContent(){
        String property = "java.io.tmpdir";
        String tempDir = System.getProperty(property);

        try {
            FileWriter fw = new FileWriter(tempDir + CONTENTLIB, true);

            fw.append("ContentLibrary.createContentString(\"" +  filename.toUpperCase() + "\" , \"" + headerEnglish + "\" , \"" + headerCzech + "\");\n");

            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void createContent(String filename){
        String property = "java.io.tmpdir";
        String tempDir = System.getProperty(property);

        String H1E = "", H1C = "";

        try {
            BufferedReader reader = new BufferedReader(new FileReader("/Bachelor/Editor/Data/"+filename+".html"));
            String line = reader.readLine();
            boolean czech = false;

            while(line != null){
                if(line.contains("h1")){
                    String[] arr = line.split("<h1>");
                    String[] rr = arr[1].split("</h1>");
                    if(czech) H1C = rr[0];
                    else H1E = rr[0];
                    czech = !czech;
                }
                line = reader.readLine();
            }
            reader.close();

            FileWriter fw = new FileWriter(tempDir + CONTENTLIB, true);

            fw.append("ContentLibrary.createContentString(\"" +  filename.toUpperCase() + "\" , \"" + H1E + "\" , \"" + H1C + "\");\n");

            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void decomposeHTML(){
        String property = "java.io.tmpdir";
        String tempDir = System.getProperty(property);

        ServerConection sc = new ServerConection("/info/"+filename, tempDir + filename);
        sc.downloadFromServer();

        try {
            BufferedReader reader = new BufferedReader(new FileReader(tempDir + filename));
            reader.readLine();
            reader.readLine();
            String line = reader.readLine();

            String[] poss = line.split("<h1>")[1].split("</h1>");
            if(poss.length == 0) headerEnglish = "";
            else headerEnglish = poss[0];
            line = reader.readLine();

            imageSourceEnglish = line.split("src=\"")[1].split("\"")[0];
            imageAltEnglish = line.split("alt=\"")[1].split("\"")[0];
            reader.readLine();

            line = reader.readLine();
            textInEnglish = "";
            textInEnglish += line;
            line = reader.readLine();
            while (!line.contains("</div>")){
                textInEnglish += "\n";
                textInEnglish += line;
                line = reader.readLine();
            }
            reader.readLine();

            line = reader.readLine();
            linksEnglish = "";
            linksEnglish += line.split("href=\"")[1].split("\"")[0];
            line = reader.readLine();
            while (!line.contains("</ul>")){
                linksEnglish += ", ";
                linksEnglish += line.split("href=\"")[1].split("\"")[0];
                line = reader.readLine();
            }
            reader.readLine();
            reader.readLine();
            reader.readLine();
            reader.readLine();
            reader.readLine();

            line = reader.readLine();
            poss = line.split("<h1>")[1].split("</h1>");
            if(poss.length == 0) headerCzech = "";
            else headerCzech = poss[0];
            line = reader.readLine();

            imageSourceCzech = line.split("src=\"")[1].split("\"")[0];
            imageAltCzech = line.split("alt=\"")[1].split("\"")[0];
            reader.readLine();

            line = reader.readLine();
            textInCzech = "";
            textInCzech += line;
            line = reader.readLine();
            while (!line.contains("</div>")){
                textInCzech += "\n";
                textInCzech += line;
                line = reader.readLine();
            }
            reader.readLine();

            line = reader.readLine();
            linksCzech = "";
            linksCzech += line.split("href=\"")[1].split("\"")[0];
            line = reader.readLine();
            while (!line.contains("</ul>")){
                linksCzech += ", ";
                linksCzech += line.split("href=\"")[1].split("\"")[0];
                line = reader.readLine();
            }

            reader.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getTextInEnglish() {
        return textInEnglish;
    }

    public String getTextInCzech() {
        return textInCzech;
    }

    public String getLinksEnglish() {
        return linksEnglish;
    }

    public String getLinksCzech() {
        return linksCzech;
    }

    public String getHeaderEnglish() {
        return headerEnglish;
    }

    public String getHeaderCzech() {
        return headerCzech;
    }

    public String getFilename() {
        return filename;
    }

    public String getImageSourceCzech() {
        return imageSourceCzech;
    }

    public String getImageAltCzech() {
        return imageAltCzech;
    }

    public String getImageSourceEnglish() {
        return imageSourceEnglish;
    }

    public String getImageAltEnglish() {
        return imageAltEnglish;
    }
}
