/*
    JsonManip.java
    Jiří Ryšavý
    11.07.2019
    Editor modelu Pražského hradu. (Bachelor Thesis)
*/
package Bachelor.Editor.Manipulation.Json;

import Bachelor.Editor.Graph.Edge;
import Bachelor.Editor.Graph.Point;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Třída určená k načítání/ukládání grafu procházky z/do json souboru
 */
public class JsonManip {
    private static final String FILENAME = "tour.json";

    //jednotlivé indexy spojené se svými souřadnicemi
    private HashMap<Integer, Point> indexes = new HashMap<>();
    //jednotlivé indexy spojené se svými hranami
    private HashMap<Integer, Edge> graph = new HashMap<>();
    //jednotlivé indexy spojené s texty
    private HashMap<Long, String> texts = new HashMap<>();

    /**
     * Vytvoří z uloženého grafu json soubor
     */
    public void Encode(){
        String property = "java.io.tmpdir";
        String tempDir = System.getProperty(property);

        // vytvoření JSON objektu
        JSONObject jo = new JSONObject();

        Map m = new LinkedHashMap();

        //ukládání textů jako hash mapy
        for (Long i : texts.keySet()){
            m.put(texts.get(i), i.longValue());
        }
        jo.put("texts", m);

        JSONArray ja = new JSONArray();

        // hrany a vrcholy jsou uloženy pouze jako pole
        for (Edge e : graph.values()){
            ja.add(new Long(e.getFrom()));
            ja.add(new Long(e.getTo()));
        }

        jo.put("indicies", ja);

        ja = new JSONArray();

        for (Point e : indexes.values()){
            ja.add(new Double(e.getX()));
            ja.add(new Double(e.getY()));
            ja.add(new Double(e.getZ()));
        }
        jo.put("vertices", ja);

        //Zápis json objektu do souboru
        PrintWriter pw = null;
        try {
            pw = new PrintWriter(tempDir + FILENAME);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        pw.write(jo.toJSONString());

        pw.flush();
        pw.close();
    }

    /**
     *  Načte z JSON souboru graf do jednotlivých hash map.
     */
    public void Decode(){
        String property = "java.io.tmpdir";
        String tempDir = System.getProperty(property);

        //Načtení souboru do JSON objektu
        Object obj = null;
        try {
            obj = new JSONParser().parse(new FileReader(tempDir + FILENAME));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        JSONObject jo = (JSONObject) obj;

        // čtení textů z objektu
        Map object = ((Map)jo.get("texts"));

        Iterator<Map.Entry> itr1 = object.entrySet().iterator();
        while (itr1.hasNext()) {
            Map.Entry pair = itr1.next();
            texts.put((Long)pair.getValue(), (String) pair.getKey());
        }

        //získání indexů z grafu (hran)
        JSONArray ja = (JSONArray) jo.get("indicies");

        Iterator itr2 = ja.iterator();
        int it = 0;

        //zápis hran do hash mapy
        while (itr2.hasNext())
        {
            long from = ((long) itr2.next());
            long to = ((long) itr2.next());
            graph.put(new Integer(it), new Edge(from, to));
            it++;
        }

        //získání souřadnic jednotlivých indexů
        ja = (JSONArray) jo.get("vertices");

        itr2 = ja.iterator();
        it = 0;

        //zápis bodů do hash mapy
        while (itr2.hasNext())
        {
            double x = ((double) itr2.next());
            double y = ((double) itr2.next());
            double z = ((double) itr2.next());
            indexes.put(new Integer(it), new Point(x,y,z));
            it++;
        }
    }

    //Následují některé gettery pro třídu.

    public HashMap<Integer, Point> getIndexes() {
        return indexes;
    }

    public HashMap<Integer, Edge> getGraph() {
        return graph;
    }

    public HashMap<Long, String> getTexts() {
        return texts;
    }
}


