/*
    Solver.java
    Jiří Ryšavý
    11.07.2019
    Editor modelu Pražského hradu. (Bachelor Thesis)
*/
package Bachelor.Editor.Manipulation.Dae;

import Bachelor.Editor.Conection.ServerConection;
import Bachelor.Editor.Graph.Edge;
import Bachelor.Editor.Graph.Point;
import Bachelor.Editor.Manipulation.Html.CreateHtmlInfo;
import Bachelor.Editor.Manipulation.Json.JsonManip;
import Bachelor.Editor.UI.Title.Title;
import Jama.Matrix;

import java.io.*;
import java.lang.reflect.Array;
import java.util.*;

import static java.util.Map.Entry.comparingByValue;
import static java.util.stream.Collectors.toMap;

/**
 * Třída určená pro zjištění požadavků uživatele a jejich zpracování.
 */
public class Solver {
    //Jednotlivé záznamy z ids.info uložené do hash mapy podle řádků v souboru.
    HashMap<Integer, IDData> objects = new HashMap<>();
    private double rx, ry, rz, tx, ty, tz, sx, sy, sz;
    private String px, py, pz;
    private Title infoCube;
    //Určuje zvolený řádek od uživatele.
    private int selected;
    //Change je bool hodnota určující, zda proběhla změna v transformaci či ne. Důležité pro rozhodování, zda objekt odstranit, nebo změnit.
    //Text je bool hodnota určující, zda bude mít model popisek, nebo ne.
    private boolean text;
    //Načítá graf z json souboru.
    private JsonManip graph = new JsonManip();
    private ArrayList<String> names = new ArrayList<>();
    //enum pro jednodušší porozumění spojení ids.info a IDData.
    private enum indexies{
        NAME, NODEID, INFORMATION, ID, ROTX, ROTY, ROTZ, TRAX, TRAY, TRAZ, SCAX, SCAY, SCAZ, PATX, PATY, PATZ, REST
    }
    private static final String IDS = "/data/scene/ids.info";
    private static final String IDS_LOCAL = "ids.info";
    private static final String CONTENTLIB_LOCAL = "ContentLib.js";
    private static final String CONTENTLIB1 = "/src/strings/ContentLibrary.js";
    private static final String CONTENTLIB2 = "/dist/js/strings/ContentLibrary.js";
    private static final String CASTLE = "/data/scene/prague_castle.dae";
    private static final String CASTLE_LOCAL = "castle.dae";
    private static final String TOUR_LOCAL = "tour.json";
    private static final String TOUR = "/data/tour/tour.json";

    private String fileToAdd = "";
    private String nameToAdd = "";
    private String textInEnglish;
    private String textInCzech;
    private String linksEnglish;
    private String linksCzech;
    private String headerEnglish;
    private String headerCzech;
    private String filename = "";
    private String imageSource;
    private String imageAlt;

    /**
     * Konstruktor pouze zavolá načítání objektů.
     */
    public Solver() {
        loadObjects();
    }

    /**
     * Přidává objekt do modelu. Pokud je metoda volána z replace a není soubor pro přidání, byl model pozměněn a není tedy co přidat.
     * Pokud existuje model k přidání, provede se sloučení přidávaného souboru a souboru s celým modelem.
     * Uloží se nový záznam do IDData, aby se zapsal do ids.info. V případě, že je i potřeba text pro objekt, vytvoří se příslušné HTML.
     * Pokračuje se k odeslání.
     */
    public void add(){
        CreateHtmlInfo chi = null;

        String property = "java.io.tmpdir";
        String tempDir = System.getProperty(property);

        Merge m = new Merge(tempDir + CASTLE_LOCAL, fileToAdd, createMatrix());
        m.start();

        int nodeID = -1;
        if(!px.equals("") && !py.equals("") && !pz.equals("")) nodeID = addToGraph(nodeID);

        if(text){
            chi = new CreateHtmlInfo(infoCube.getTextInEn().getText(),
                    infoCube.getTextInCZ().getText(), infoCube.getLinksEn().getText(),
                    infoCube.getLinksCz().getText(), infoCube.getHeaderEn().getText(),
                    infoCube.getHeaderCz().getText(), m.getLast(),
                    infoCube.getImageLinkCz().getText(), infoCube.getImageAltCz().getText(),
                    infoCube.getImageLinkEn().getText(), infoCube.getImageAltEn().getText());
            chi.createHTML();
            chi.createContent();
        }

        objects.put(-1, new IDData(m.getIds(), rx, ry, rz, sx, sy, sz, tx, ty, tz, px, py, pz, nodeID, chi != null ? chi.getFilename() : "", nameToAdd, m.getLast()));

        end(m);
    }

    /**
     * Kontroluje, zda proběhla změna v transformační matici. Pokud ne, zavolá se odstranění objektu z .dae souboru.
     */
    public void replace() {

        IDData selectedItem = objects.get(selected);

        nameToAdd = selectedItem.getName();

        String property = "java.io.tmpdir";
        String tempDir = System.getProperty(property);

        if(!fileToAdd.equals("")) {
            CreateHtmlInfo chi = new CreateHtmlInfo(selectedItem.getNodeId());
            chi.removeFromContent();

            Remove r = new Remove(tempDir + CASTLE_LOCAL, selectedItem);
            r.start();

            removeFromGraph(selectedItem.getID());
            objects.remove(selected);

            text = true;
            add();
        } else {
            Merge m = new Merge(tempDir + CASTLE_LOCAL, fileToAdd, createMatrix());
            m.setIds(new ArrayList<>(Arrays.asList(selectedItem.getHeaders())));
            m.replace(selectedItem.getNodeId());

            removeFromGraph(selectedItem.getID());
            int nodeID = -1;
            if(!px.equals("") && !py.equals("") && !pz.equals("")) nodeID = addToGraph(nodeID);

            if(text){
                CreateHtmlInfo chi = new CreateHtmlInfo(infoCube.getTextInEn().getText(),
                        infoCube.getTextInCZ().getText(), infoCube.getLinksEn().getText(),
                        infoCube.getLinksCz().getText(), infoCube.getHeaderEn().getText(),
                        infoCube.getHeaderCz().getText(), selectedItem.getNodeId(),
                        infoCube.getImageLinkCz().getText(), infoCube.getImageAltCz().getText(),
                        infoCube.getImageLinkEn().getText(), infoCube.getImageAltEn().getText());
                chi.removeFromContent();
                chi.createHTML();
                chi.createContent();
                selectedItem.setInformationFile(chi.getFilename());
            }

            selectedItem.setID(nodeID);
            selectedItem.setPath(new String[]{px, py, pz});
            selectedItem.setRotation(new double[]{rx, ry, rz});
            selectedItem.setScale(new double[]{sx, sy, sz});
            selectedItem.setTranslate(new double[]{tx, ty, tz});
            end(m);
        }
    }

    public void remove(){

        CreateHtmlInfo chi = new CreateHtmlInfo(objects.get(selected).getNodeId());
        chi.removeFromContent();

        String property = "java.io.tmpdir";
        String tempDir = System.getProperty(property);

        Remove r = new Remove(tempDir + CASTLE_LOCAL, objects.get(selected));;
        r.start();

        removeFromGraph(objects.get(selected).getID());

        objects.remove(selected);
        end(new Merge("", "", ""));
    }

    /**
     * Vytváří transformační matici z jednotlivých transformací.
     * @return matice jako string "a11 a12 a13 a14 a21 a22 a23 a24 a31 a32 a33 a34 a41 a42 a43 a44"
     */
    private String createMatrix() {
        String matrix;

        // rotační matice kolem osy x, kde alpha určuje úhel natočení:
        // 1 0          0           0
        // 0 cos(alpha) -sin(alpha) 0
        // 0 sin(alpha) cos(alpha)  0
        // 0 0          0           1
        Matrix rotX = new Matrix(new double[][]{{1.0, 0.0, 0.0, 0.0}, {0.0, Math.cos(rx), -Math.sin(rx), 0.0}, {0.0, Math.sin(rx), Math.cos(rx), 0.0}, {0.0, 0.0, 0.0, 1.0}});
        //podobně pro osy y a z
        Matrix rotY = new Matrix(new double[][]{{Math.cos(ry), 0.0, Math.sin(ry), 0.0}, {0.0, 1.0, 0.0, 0.0}, {-Math.sin(ry), 0.0, Math.cos(ry), 0.0}, {0.0, 0.0, 0.0, 1.0}});
        Matrix rotZ = new Matrix(new double[][]{{Math.cos(rz), -Math.sin(rz), 0.0, 0.0}, {Math.sin(rz), Math.cos(rz), 0.0, 0.0}, {0.0, 0.0, 1.0, 0.0}, {0.0, 0.0, 0.0, 1.0}});

        // rotační matice je násobkem jednotlivých x, y a z rotací
        Matrix rot = (rotX.times(rotY)).times(rotZ);
        // matice posunu, kde tx, ty a tz jsou jednotlivé posuny v daných osách:
        // 1 0 0 tx
        // 0 1 0 ty
        // 0 0 1 tz
        // 0 0 0 1
        Matrix tra = new Matrix(new double[][]{{1.0, 0.0, 0.0, tx}, {0.0, 1.0, 0.0, ty}, {0.0, 0.0, 1.0, tz}, {0.0, 0.0, 0.0, 1.0}});

        // matice zvětšení, kde sx, sy a sz jsou jednotlivé zvětšení v daných osách:
        // sx 0  0  0
        // 0  sy 0  0
        // 0  0  sz 0
        // 0  0  0  1
        Matrix sca = new Matrix(new double[][]{{sx, 0.0, 0.0, 0.0}, {0.0, sy, 0.0, 0.0}, {0.0, 0.0, sz, 0.0}, {0.0, 0.0, 0.0, 1.0}});
        //Počátek je pouze jednotková matice.
        Matrix origin = new Matrix(new double[][]{{1.0, 0.0, 0.0, 0.0}, {0.0, 1.0, 0.0, 0.0}, {0.0, 0.0, 1.0, 0.0}, {0.0, 0.0, 0.0, 1.0}});


        //výsledná transformační matice je pouze násobení dílčích transformačních matic
        double[][] fin = (((tra.times(rot)).times(sca)).times(origin)).getArray();
        
        matrix = "";
        for (double[] aFin : fin) {
            for (int j = 0; j < fin.length; j++) {
                matrix += aFin[j] + " ";
            }
        }
        
        return matrix;
    }

    /**
     * Přidává bod do grafu. Nalezne nejkratší vzdálenost vůči jednotlivým bodům, které už v grafu jsou a s tímto bodem se spojí hranou.
     * @param newNode reprezentuje přidávaný bod do grafu
     * @return id nového bodu v grafu, důležité pro násladné ukládání do ids.info
     */
    private Integer addToGraph(Integer newNode){
        Integer max = Integer.MIN_VALUE;

        for (Integer i: graph.getIndexes().keySet()){
            if(i.compareTo(max) > 0){
                max = i;
            }
        }
        newNode = max+1;

        //vytvoření nového uzlu
        Point pointToAdd = new Point(Double.parseDouble(px), Double.parseDouble(py), Double.parseDouble(pz));
        double minLength = Double.MAX_VALUE;
        Long l = new Integer(-1).longValue();

        //hledání nejkratší hrany do grafu
        for (Integer key: graph.getIndexes().keySet()) {
            double tmpLen = graph.getIndexes().get(key).distance(pointToAdd);
            if(tmpLen < minLength){
                l = key.longValue();
                minLength = tmpLen;
            }
        }

        //ukládání do grafu
        graph.getIndexes().put(newNode, pointToAdd);
        graph.getTexts().put(newNode.longValue(), nameToAdd);
        graph.getGraph().put(-1, new Edge(newNode.longValue(), l));

        return newNode;
    }

    private void removeFromGraph(Integer toRemove){
        graph.getTexts().remove(toRemove.longValue());

        ArrayList<Integer> edges = new ArrayList<Integer>();

        for (Integer i : graph.getGraph().keySet()){
            if(graph.getGraph().get(i).getTo() == toRemove || graph.getGraph().get(i).getFrom() == toRemove){
                edges.add(i);
            }
        }

        if(edges.size() == 1) {
            graph.getIndexes().remove(toRemove);
            graph.getGraph().remove(edges.get(0));
        }
    }

    /**
     * Odesílá jednotlivé soubory na server.
     * @param m informace o spojených souborech
     */
    private void sendToServer(Merge m){
        String property = "java.io.tmpdir";
        String tempDir = System.getProperty(property);

        //model hradu .dae
        ServerConection sc = new ServerConection(CASTLE, tempDir + CASTLE_LOCAL);
        sc.sendToServer();

        //ids.info
        sc.setServerFile(IDS);
        sc.setLocalFile(tempDir + IDS_LOCAL);
        sc.sendToServer();

        //graf scény v json souboru
        sc.setServerFile(TOUR);
        sc.setLocalFile(tempDir + TOUR_LOCAL);
        sc.sendToServer();

        //contentlib obsahuje informace o objektech s popisky
        sc.setServerFile(CONTENTLIB1);
        sc.setLocalFile(tempDir + CONTENTLIB_LOCAL);
        sc.sendToServer();

        sc.setServerFile(CONTENTLIB2);
        sc.sendToServer();

        //Odesílání souborů s texturami, jsou-li nějaké
        for (String fname : m.getTextures()) {
            sc.setServerFile("/data/scene/" + fname);
            File fil = new File(fileToAdd);
            sc.setLocalFile(fil.getParent() + "\\" + fname);
            sc.sendImageToServer();
        }

        //vyčištění tempu
        File f = new File(tempDir + CASTLE_LOCAL);
        f.delete();
        f = new File(tempDir + TOUR_LOCAL);
        f.delete();
        f = new File(tempDir + IDS_LOCAL);
        f.delete();
        f = new File(tempDir + CONTENTLIB_LOCAL);
        f.delete();
    }

    private void sortObjects(){
        HashMap<Integer, IDData> sorted = objects
                .entrySet()
                .stream()
                .sorted(comparingByValue())
                .collect(
                        toMap(e -> e.getKey(), e -> e.getValue(), (e1, e2) -> e2,
                                LinkedHashMap::new));
        objects = sorted;
    }

    /**
     * Vytváří ids.info z hash mapy s objekty a json soubor s grafem.
     * @param m informace o spojených souborech
     */
    private void end(Merge m){
        String property = "java.io.tmpdir";
        String tempDir = System.getProperty(property);

        //vytvoření json souboru z grafu
        graph.Encode();
        sortObjects();

        // vytváření ids.info
        FileWriter fw;
        try {
            //smazání původního souboru
            fw = new FileWriter(tempDir + IDS_LOCAL, false);
            fw.append("");
            fw.close();

            //zápis nového souboru
            fw = new FileWriter(tempDir + IDS_LOCAL, true);
            for (IDData d :objects.values()) {
                fw.append(d.toString() + "\n");
            }
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        sendToServer(m);
    }

    /**
     * Načítání jednotlivých částí z ids.info do hash mapy s objekty a načtení grafu z json souboru.
     */
    private void loadObjects(){
        String property = "java.io.tmpdir";
        String tempDir = System.getProperty(property);

        BufferedReader reader;
        double rx, ry, rz, sx, sy, sz, tx, ty, tz;
        String px, py, pz;
        int id, lineNum = 0;
        //načtení grafu z json souboru
        graph.Decode();
        
        try {
            reader = new BufferedReader(new FileReader(tempDir + IDS_LOCAL));

            //Procházení ids.info a ukládání jednotlivých řádek do hash mapy s pomocí enum
            String line = reader.readLine();

            while (line != null){
                String[] parsedLine = line.split("/");

                names.add(parsedLine[indexies.NAME.ordinal()]);
                ArrayList<String> headers = new ArrayList<>();

                //Načítání jednotlivých ID pro tagy
                for(int i = indexies.REST.ordinal(); i < parsedLine.length; i++){
                    headers.add(parsedLine[i]);
                }

                //Načtení hodnot pro transformace jako double, ne string
                rx = Double.parseDouble(parsedLine[indexies.ROTX.ordinal()]);
                ry = Double.parseDouble(parsedLine[indexies.ROTY.ordinal()]);
                rz = Double.parseDouble(parsedLine[indexies.ROTZ.ordinal()]);
                sx = Double.parseDouble(parsedLine[indexies.SCAX.ordinal()]);
                sy = Double.parseDouble(parsedLine[indexies.SCAY.ordinal()]);
                sz = Double.parseDouble(parsedLine[indexies.SCAZ.ordinal()]);
                tx = Double.parseDouble(parsedLine[indexies.TRAX.ordinal()]);
                ty = Double.parseDouble(parsedLine[indexies.TRAY.ordinal()]);
                tz = Double.parseDouble(parsedLine[indexies.TRAZ.ordinal()]);
                px = parsedLine[indexies.PATX.ordinal()];
                py = parsedLine[indexies.PATY.ordinal()];
                pz = parsedLine[indexies.PATZ.ordinal()];
                id = Integer.parseInt(parsedLine[indexies.ID.ordinal()]);

                //Načítání jednotlivých ID pro tagy
                String[] arr = new String[headers.size()];
                for(int i=0; i < headers.size(); i++){
                    arr[i] = headers.get(i);
                }

                //přidání záznamu do hash mapy
                IDData toAdd = new IDData(arr, rx, ry, rz, sx, sy, sz, tx, ty, tz, px, py, pz, id,
                        parsedLine[indexies.INFORMATION.ordinal()], parsedLine[indexies.NAME.ordinal()], parsedLine[indexies.NODEID.ordinal()]);

                objects.put(lineNum, toAdd);
                
                line = reader.readLine();
                lineNum++;
            }

            reader.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Následují některé gettery a settery třídy

    public void setRx(double rx) {
        this.rx = rx;
    }

    public void setRy(double ry) {
        this.ry = ry;
    }

    public void setRz(double rz) {
        this.rz = rz;
    }

    public void setTx(double tx) {
        this.tx = tx;
    }

    public void setTy(double ty) {
        this.ty = ty;
    }

    public void setTz(double tz) {
        this.tz = tz;
    }

    public void setSx(double sx) {
        this.sx = sx;
    }

    public void setSy(double sy) {
        this.sy = sy;
    }

    public void setSz(double sz) {
        this.sz = sz;
    }

    public void setPx(String px) {
        this.px = px;
    }

    public void setPy(String py) {
        this.py = py;
    }

    public void setPz(String pz) {
        this.pz = pz;
    }

    public void setText(boolean text) {
        this.text = text;
    }

    public void setTextInEnglish(String textInEnglish) {
        this.textInEnglish = textInEnglish;
    }

    public void setTextInCzech(String textInCzech) {
        this.textInCzech = textInCzech;
    }

    public void setLinksEnglish(String linksEnglish) {
        this.linksEnglish = linksEnglish;
    }

    public void setLinksCzech(String linksCzech) {
        this.linksCzech = linksCzech;
    }

    public void setHeaderEnglish(String headerEnglish) {
        this.headerEnglish = headerEnglish;
    }

    public void setHeaderCzech(String headerCzech) {
        this.headerCzech = headerCzech;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public void setImageSource(String imageSource) {
        this.imageSource = imageSource;
    }

    public void setImageAlt(String imageAlt) {
        this.imageAlt = imageAlt;
    }

    public void setSelected(int selected) {
        this.selected = selected;
    }

    public ArrayList<String> getNames() {
        return names;
    }

    public void setFileToAdd(String fileToAdd) {
        this.fileToAdd = fileToAdd;
    }

    public void setNameToAdd(String nameToAdd) {
        this.nameToAdd = nameToAdd;
    }

    public HashMap<Integer, IDData> getObjects() {
        return objects;
    }

    public void setInfoCube(Title infoCube) {
        this.infoCube = infoCube;
    }
}
