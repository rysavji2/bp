/*
    Merge.java
    Jiří Ryšavý
    11.07.2019
    Editor modelu Pražského hradu. (Bachelor Thesis)
*/
package Bachelor.Editor.Manipulation.Dae;


import java.io.*;
import java.util.ArrayList;

/**
 * Třída pro funkci spojení dvou .dae souborů.
 */
public class Merge {

    private String file1, file2;
    private static final String newLine = "\n";
    //Pole pro ukládání cest k souborům textur.
    private ArrayList<String> textures;
    private String matrix;
    //pole pro ukládání id pro jednotlivé tagy .dae souboru. Důležité pro zápis do ids.info
    private ArrayList<String> ids = new ArrayList<>();
    private boolean first = true;
    //Dočasný soubor pro výsledný model.
    private static final String TMP_FILE = "tmp.dae";
    private String last;

    /**
     * Konstruktor pro spojování.
     * @param file1 první soubor pro spojení
     * @param file2 druhý soubor pro spojení
     * @param matrix transformační matice pro přidávaný objekt (uložený v druhém souboru).
     */
    public Merge(String file1, String file2, String matrix){
        this.file1 = file1;
        this.file2 = file2;
        textures = new ArrayList<>();
        this.matrix = matrix;

    }

    /**
     * Spojování modelu z druhého souboru a celého modelu hradu. Prochází soubor s hradem a kopíruje ho do výsledného souboru.
     * Při dokončení zápisu jednoho tagu se zápis souboru s hradem ukončí a přepne se zápis na model, který se přidává.
     * V případě, že se jedná o tag s identifikátorm 'node', použije se zápis matice u přidávaného modelu.
     * Při načítání tagu image ukládá cesty k obrázkům. Určuje texturovací obrázky.
     * @param reader1 čte první soubor, soubor s hradem
     * @param reader2 čte druhý soubor, soubor přidávaný
     * @param fw zapisuje do výstupního souboru
     * @param tag určuje právě přepisovaný tag .dae souboru
     * @param end určuje, zda je zapisovaný tag ukončující, či ne.
     */
    private void tagWriter(BufferedReader reader1, BufferedReader reader2, FileWriter fw, String tag, boolean end){
        String endTag = tag+"/";

        try {
            reader1.mark(20);
            String line = reader1.readLine();
            boolean first = true;
            while (!line.contains(tag)) {
                fw.append(line);
                fw.append(newLine);
                line = reader1.readLine();
            }

            if(line.contains(endTag) && !end) reader1.reset();

            reader2.mark(20);
            line = reader2.readLine();

            while (!line.contains(tag)) {
                if(line.contains(" id") && (line.contains("image") || line.contains("effect") || line.contains("material") || line.contains("geometry") || line.contains("node"))){
                    String id = line.split("\"")[1];
                    if(line.contains("node") && first){
                        last = id;
                    }
                    ids.add(id);
                }
                if(line.contains("init_from")){
                    String id = line.split(">")[1];
                    String filename = id.split("<")[0];
                    textures.add(filename);
                }
                if(line.contains("matrix") && first){
                    fw.append("<matrix sid=\"transform\">"+matrix+"</matrix>");
                    first = !first;
                }
                else fw.append(line);
                fw.append(newLine);
                line = reader2.readLine();
            }

            if(line.contains(endTag) && !end) reader2.reset();

            if(end) fw.append("  </");
            else fw.append("  <");
            fw.append(tag);
            if(tag.equals("visual_scene") && !end) fw.append(" id=\"Scene\" name=\"Scene\"");

            fw.append(">");
            fw.append(newLine);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Kopíruje hlavičku .dae souboru a posune oba kurzory souborů na část s modelem.
     * @param reader1 kurzor prvního souboru
     * @param reader2 kurzor druhého souboru
     * @param fw kurzor zapisovaného souboru
     */
    private void copyHeader(BufferedReader reader1, BufferedReader reader2, FileWriter fw){
        try {
            String line = reader2.readLine();

            while (!line.contains("</asset>")){

                fw.append(line);
                fw.append(newLine);
                line = reader2.readLine();
            }
            fw.append(line);
            fw.append(newLine);

            line = reader1.readLine();
            while (!line.contains("</asset>")) line = reader1.readLine();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Hlavní metoda zápisu výsledného souboru.
     * Vytvoří stream pro čtení ze dvou souborů. Přepíše hlavičku do výsledného souboru.
     * Následně volá zápis tagů do výsledného .dae souboru s jednotlivými tagy, které jsou pro .dae důležité.
     * Po provedení sloučení souborů se výsledný uloží jako hrad a soubory, ze kterých se četlo, se smažou.
     */
    public void start(){

        String property = "java.io.tmpdir";
        String tempDir = System.getProperty(property);

        BufferedReader reader1;
        BufferedReader reader2;

        FileWriter fw;
        FileWriter f;

        try {
            reader1 = new BufferedReader(new FileReader(file1));
            reader2 = new BufferedReader(new FileReader(file2));
            fw = new FileWriter(tempDir + TMP_FILE);
            fw.write("");
            fw.close();
            fw = new FileWriter(tempDir + TMP_FILE, true);

            copyHeader(reader1, reader2, fw);

            tagWriter(reader1, reader2, fw,"library_images",false);
            tagWriter(reader1, reader2, fw,"library_images", true);

            tagWriter(reader1, reader2, fw,"library_effects",false);
            tagWriter(reader1, reader2, fw,"library_effects", true);

            tagWriter(reader1, reader2, fw,"library_materials",false);
            tagWriter(reader1, reader2, fw,"library_materials", true);

            tagWriter(reader1, reader2, fw,"library_geometries",false);
            tagWriter(reader1, reader2, fw,"library_geometries", true);

            tagWriter(reader1, reader2, fw,"library_controllers",false);
            tagWriter(reader1, reader2, fw,"library_controllers", true);

            tagWriter(reader1, reader2, fw,"library_visual_scenes",false);

            tagWriter(reader1, reader2, fw,"visual_scene",false);
            tagWriter(reader1, reader2, fw,"visual_scene", true);

            tagWriter(reader1, reader2, fw,"library_visual_scenes", true);

            reader1.close();

            String line = reader2.readLine();

            while (line != null){
                fw.append(line);
                fw.append(newLine);
                line = reader2.readLine();
            }

            reader2.close();
            fw.close();

            File fil = new File(file1);
            fil.delete();

            File fl = new File(tempDir + TMP_FILE);
            fil = new File(file1);
            fl.renameTo(fil);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Tato metoda se volá v případě, kdy se pouze změnil parametr nějakého objektu.
     * Soubor bude změněn pouze v transformační matici jednoho objektu.
     * @param id identifikátor node, pro který se bude provádět změna.
     */
    public void replace(String id){
        String property = "java.io.tmpdir";
        String tempDir = System.getProperty(property);

        BufferedReader reader;
        FileWriter fw;
        last = id;
        boolean first = true;

        try {
            reader = new BufferedReader(new FileReader(file1));
            fw = new FileWriter(tempDir + TMP_FILE);
            fw.write("");
            fw.close();
            fw = new FileWriter(tempDir + TMP_FILE, true);

            String line = reader.readLine();

            while (line != null){
                if(line.contains("node") && line.contains(id) && first){
                    fw.append(line);
                    fw.append("\n");
                    reader.readLine();
                    fw.append("<matrix sid=\"transform\">" + matrix + "</matrix>");
                    first = false;
                }
                else fw.append(line);
                fw.append("\n");
                line = reader.readLine();
            }

            reader.close();
            fw.close();

            File fil = new File(file1);
            fil.delete();

            File fl = new File(tempDir + TMP_FILE);
            fil = new File(file1);
            fl.renameTo(fil);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Následují gettery/settery pro některé části třídy.
    public ArrayList<String> getTextures() {
        return textures;
    }

    public String[] getIds() {

        String[] arr = new String[ids.size()];
        for(int i=0; i < ids.size(); i++){
            arr[i] = ids.get(i);
        }

        return arr;
    }

    public void setIds(ArrayList<String> ids) {
        this.ids = ids;
    }

    public String getLast() {
        return last;
    }
}
