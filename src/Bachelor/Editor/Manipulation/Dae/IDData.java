/*
    IDData.java
    Jiří Ryšavý
    11.07.2019
    Editor modelu Pražského hradu. (Bachelor Thesis)
*/
package Bachelor.Editor.Manipulation.Dae;

/**
 * Třída reprezentujjící jeden řádek souboru ids.info, tedy jeden objekt modelu. Uchovává důležité informace pro správu jednotlivých částí modelu.
 */
public class IDData implements Comparable<IDData>{
    private String[] headers;
    private double[] rotation;
    private double[] scale;
    private double[] translate;
    private String[] path;
    private int ID;
    private String informationFile;
    private String name, nodeId;

    /**
     * Konstruktor IDData.
     * @param headers id u jednotlivých tagů v souboru dae pro daný objekt
     * @param rotationX rotace kolem osy x objektu
     * @param rotationY rotace kolem osy y objektu
     * @param rotationZ rotace kolem osy z objektu
     * @param scaleX zvětšení podle osy x objektu
     * @param scaleY zvětšení podle osy y objektu
     * @param scaleZ zvětšení podle osy z objektu
     * @param translateX posun na ose x objektu
     * @param translateY posun na ose y objektu
     * @param translateZ posun na ose z objektu
     * @param pathX souřednice x bodu grafu
     * @param pathY souřednice y bodu grafu
     * @param pathZ souřadnice z bodu grafu
     * @param ID identifikační číslo v grafu
     * @param informationFile nabívá hodnoty filename/n podle toho, zda existuje informační soubor, nebo ne.
     * @param name název, pod kterým bude objekt reprezentován v tabulce objektů
     * @param nodeId id pro node v souboru dae. Důežité pro funkčnost práce s objekty složené z více částí.
     */
    public IDData(String[] headers, double rotationX, double rotationY, double rotationZ,
                  double scaleX, double scaleY, double scaleZ,
                  double translateX, double translateY, double translateZ,
                  String pathX, String pathY, String pathZ,
                  int ID, String informationFile, String name, String nodeId) {
        this.headers = headers;
        this.rotation = new double[]{rotationX, rotationY, rotationZ};
        this.scale = new double[]{scaleX, scaleY, scaleZ};
        this.translate = new double[]{translateX, translateY, translateZ};
        this.path = new String[]{pathX, pathY, pathZ};
        this.ID = ID;
        this.informationFile = informationFile;
        this.name = name;
        this.nodeId = nodeId;
    }

    /**
     * Vytváří text, složený přesně tak, jak ho chceme v ids.info
     * @return string, který bude zaznamenávat objekt v ids.info
     */
    @Override
    public String toString() {
        String tags = "";
        for (int i = 0; i < headers.length; i++){
            tags += headers[i];
            if(i+1 != headers.length) tags += "/";
        }
        
        return name + "/" + nodeId + "/" + informationFile + "/" + ID + "/" +
                rotation[0] + "/" + rotation[1] + "/" + rotation[2] + "/" + 
                translate[0] + "/" + translate[1] + "/" + translate[2] + "/" +
                scale[0] + "/" + scale[1] + "/" + scale[2] + "/" +
                path[0] + "/" + path[1] + "/" + path[2] + "/" +
                tags;
    }

    //Následují gettery a settery pro jednotlivé části z iddata
    public String[] getHeaders() {
        return headers;
    }

    public String getName() {
        return name;
    }

    public void setRotation(double[] rotation) {
        this.rotation = rotation;
    }

    public void setScale(double[] scale) {
        this.scale = scale;
    }

    public void setTranslate(double[] translate) {
        this.translate = translate;
    }

    public void setPath(String[] path) {
        this.path = path;
    }

    public void setHeaders(String[] headers) {
        this.headers = headers;
    }

    public void setID(int ID) {
        this.ID = ID;
    }



    public double[] getRotation() {
        return rotation;
    }

    public double[] getScale() {
        return scale;
    }

    public double[] getTranslate() {
        return translate;
    }

    public String[] getPath() {
        return path;
    }

    public String getNodeId() {
        return nodeId;
    }

    public int getID() {
        return ID;
    }

    public String getInformationFile() {
        return informationFile;
    }

    public void setInformationFile(String informationFile) {
        this.informationFile = informationFile;
    }


    @Override
    public int compareTo(IDData o) {
        return name.compareTo(o.getName());
    }
}
