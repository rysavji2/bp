/*
    Remove.java
    Jiří Ryšavý
    11.07.2019
    Editor modelu Pražského hradu. (Bachelor Thesis)
*/
package Bachelor.Editor.Manipulation.Dae;

import java.io.*;

/**
 * Třída pro odstraňování objektu z .dae souborů.
 */
public class Remove {
    private static final String newLine = "\n";

    private String file;
    private static final String CASTLE = "castle.dae";
    private static final String TMP_FILE = "tmp.dae";
    private IDData object;

    /**
     * Konstruktor třídy pro odstraňování.
     *
     * @param file soubor, ze kterého budeme odebírat objekt.
     * @param object IDData objekt, ve kterém máme záznamy pro tento objekt.
     */
    public Remove(String file, IDData object) {
        this.file = file;
        this.object = object;
    }

    /**
     * Odstraňuje části z .dae. Prochází soubor a při nalezení na id, které má jít pryč, zaznamená se tag,
     * dokud se nenalezne ukončující tag, nic se nezapisuje.
     * @param reader stream, ze kterého se čte původní soubor
     * @param fw stream, do kterého se zapisuje výsledný soubor
     * @param id právě hledaný identifikátor
     */
    private void removeFromFile(BufferedReader reader, FileWriter fw, String id){

        try {
            String line = reader.readLine();

            while (!line.contains(id)) {
                fw.append(line);
                fw.append(newLine);
                line = reader.readLine();
            }

            String[] arr = line.split(" ");

            int i=0;
            String subtag = "";
            for(; i<arr.length; i++){
                if(!arr[i].equals("")){
                    subtag = arr[i];
                    break;
                }
            }

            String tag = subtag.split("<")[1];

            reader.mark(20);
            line = reader.readLine();

            while (!line.contains(tag)){
                reader.mark(20);
                line = reader.readLine();
            }
            if(!line.contains("/"+tag)) reader.reset();

            if(tag.equals("node")){
                reader.mark(20);
                line = reader.readLine();
                while (!line.contains("<"+tag) && !line.contains("</visual_scene>")){
                    reader.mark(20);
                    line = reader.readLine();
                }
                reader.reset();
            }

        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metoda, která je využívána jako hlavní spouštěč odstraňování.
     * Prochází id objektu uložená v IDData objektu a postupně je odstraňuje voláním metody removeFromFile.
     */
    public void start(){

        String property = "java.io.tmpdir";
        String tempDir = System.getProperty(property);

        BufferedReader reader;

        FileWriter fw;

        try {
            String line;
            String ids[] = null;

            reader = new BufferedReader(new FileReader(file));
            fw = new FileWriter(tempDir + TMP_FILE, true);

            for(int i=0; i < object.getHeaders().length; i++){
                removeFromFile(reader, fw, object.getHeaders()[i]);
            }

            line = reader.readLine();

            while (line != null){
                fw.append(line);
                fw.append(newLine);
                line = reader.readLine();
            }

            reader.close();
            fw.close();

            File fl = new File(tempDir + CASTLE);
            boolean a = fl.delete();
            fl = new File(tempDir + TMP_FILE);
            a = fl.renameTo(new File(tempDir + CASTLE));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
